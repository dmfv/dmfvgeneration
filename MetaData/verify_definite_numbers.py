import os,json,glob
from collections import OrderedDict


def find_xsec(dsid,control_name):

    compy=[]
    for inline in open("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/PMGxsecDB_mc16.txt","r"):
        inline=inline.split("\n")[0]
        components=inline.split("\t")
        compy=[]
        for comp in components:
            if comp.rstrip() == "":
                continue
            compy.append(comp.rstrip())
        if dsid == compy[0]:
            break
        
    return float(compy[2]),float(comp[3])

#mc15_13TeV.524688.MGPy8EG_A14N30NLO_m1200_d1p75_s0p2_LHQDF2_1LorMET.evgen.EVNT.e8470;524688;0;7.1223E-06;1.0;9.720185E-01;0.0;#UNKNOWN#


signals=[x.split("\n")[0].split(";") for x in open("my-new-signal-out.txt","r") if "mc15_13TeV" in x]

folders=glob.glob("/lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/uncertainties_production/*inclus.EVNT.log")

os.system("rm my_new_sig_out_processed.txt")
os.system("echo 'ldn/C:dataset_number/I:subprocessID/I:crossSection/D:kFactor/D:genFiltEff/D:crossSectionTotalRelUncertUP/D:crossSectionRef/C' > my_new_sig_out_processed.txt")
for log in folders:
    name=log.split("/")[-1].replace("_inclus.EVNT.log","")
    xsecRef,filterRef = None,None
    DSID=None
    refLine = None
    for sig in signals:
        if name in sig[0]:
            DSID=sig[1]
            xsecRef = float(sig[3])*1000  ## transform it into pb 
            filterRef = float(sig[5])
            refLine=sig
            break
    xsection,filtereff=None,None
    foundxsec,foundfilter=False,False
    for inline in open(log,"r"):
        inline=inline.split("\n")[0]
        if "Cross-section" in inline:
            xsection=float(inline.split("+-")[0].split("Cross-section :   ")[-1].rstrip()) ## transform it into pb
            xsection_unc=float(inline.split("+-")[1].replace("pb","").rstrip()) ## transform it into pb
            foundxsec=True
        if "MetaData: GenFiltEff =" in inline:
            filtereff=float(inline.split("+-")[0].split("MetaData: GenFiltEff =")[-1].rstrip()) ## transform it into pb   
            foundfilter=True
        if foundfilter and foundxsec:
            break

    if xsection and xsecRef:
        if (xsection-xsecRef)/xsection > 0.01:
            print("\033[91mProblem with xsection for %s : ref (%.4f) nom (%.4f)\033[0m"%(name,xsecRef,xsection))
        else:
            print("\033[95mxsection for %s is ok! \033[0m"%name)
            print("\033[94m        Uncertainty is %.4f %% \033[0m"%(xsection_unc/xsection))
            pmg_xsec,pmg_filtereff=find_xsec(DSID,name)
            if (xsection-pmg_xsec)/xsection > 0.01 or (filtereff-pmg_filtereff)/filtereff > 0.01:
                print("\033[91m        but we have a problem with the values in PMG's file %s\033[0m"%name)
                refLine[6]=str(xsection_unc/xsection)
                os.system("echo '%s' >>my_new_sig_out_processed.txt"%(";".join(refLine)))
            else:
                print("\033[92m       Enjoy the beauty of xsection consistency: ref (%.4f) nom (%.4f) pmg (%.4f)\033[0m"%(xsecRef,xsection,pmg_xsec))
    elif not xsecRef:
        print("\033[91mWhat the fuck ? Missing ref xsecs %s\033[0m"%name)
    elif not xsection:
        print("\033[91mWhat the fuck ? Missing nom xsecs %s\033[0m"%name)


os.system("echo '' >> my_new_sig_out_processed.txt")
os.system("echo '#lsetup  \"asetup AthAnalysis,21.2.263\" pyAMI' >> my_new_sig_out_processed.txt")
os.system("echo '#getMetadata.py --timestamp=\"2023-09-13 10:21:58\" --physicsGroups=\"PMG,MCGN\" --fields=\"ldn,dataset_number,subprocessID,crossSection,kFactor,genFiltEff,crossSectionTotalRelUncertUP,crossSectionRef\" --inDsTxt=\"data.txt\"' >> my_new_sig_out_processed.txt ")
