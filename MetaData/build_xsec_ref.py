import os,json


outfile = "tcmet_signals_ref.json"
infile = "my-new-signal-out.txt"


signals = {}
for inline in open(infile,'r').readlines():
    if not "mc15_13TeV" in inline:
        continue

    full_name,dsid,subproc,xsection,kfactor,genfilt,crossSecUnce,ref = inline.split('\n')[0].split(';')
    name = full_name.split("A14N30NLO_")[-1].split('_1LorMET')[0]
    mmed = float(name.split("_")[0].replace("m",""))
    delta = float(name.split("_")[1].replace("d","").replace("p","."))
    signals[name] = [mmed,delta,float(xsection)*1000000,float(genfilt)] #convret xsection into fb


with open(outfile,'w+') as f:
    json.dump(signals,f,indent = 4)
