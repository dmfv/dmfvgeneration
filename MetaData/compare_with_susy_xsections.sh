#!/bin/bash

region=( sra srb src srabc )

for reg in ${region[*]}
do
    python compare_with_susy_xsections.py -r $reg
done
