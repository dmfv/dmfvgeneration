import os,json,glob,math,sys,argparse


from collections import OrderedDict


import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt

#sys.path.append("/afs/ifh.de/user/a/alopezso/PythonScripts/")

def myfmt(x, pos):
    return '{0:.1f}'.format(x)


def matplot(inpoints,benchmark,reg,labelz,outputName):

    x,y,z=[],[],[]

    for masses,values in inpoints.items():
        #if values<-0.000001:
        #    continue
        if math.isnan(values):
            continue

        x.append(masses[0])
        y.append(masses[1])
        z.append(values)

    xlist=np.array(x)
    ylist=np.array(y)
    zlist=np.array(z)

    #fig=plt.figure()                                                                                                                                                                                       
    #    plt.axis([400,1300,0,800]) # axis ranges                                                                                                                                                           

    cnt = None
    cnt=plt.tricontourf(xlist,ylist,zlist,levels=np.linspace(min(z),max(z),100), cmap='coolwarm')

    ax = plt.gca()
    for c in cnt.collections:
        c.set_edgecolor("face")

    cbar = plt.colorbar(cnt)
    cbar.set_label(r"%s"%labelz)

    plt.scatter(xlist,ylist,s=5,facecolors='none', edgecolors='k')

    for i,_ in enumerate(zlist):
        plt.annotate('{0:.2f}'.format(zlist[i]), (xlist[i],ylist[i]), size=6)


    plt.xlabel(r"$m(\phi)$ [GeV]")
    plt.ylabel(r"$D_{\lambda 11}$")
    plt.text(0.05,0.87,r"$\sqrt{s} = 13 TeV; 139 fb^{-1}$",transform = plt.gca().transAxes, alpha=1.0, fontsize=10)
    plt.text(0.05,0.81,r"Benchmark: $%s$"%benchmark,transform = plt.gca().transAxes, alpha=1.0, fontsize=10)
    plt.text(0.05,0.75,r"Region: $%s$"%reg,transform = plt.gca().transAxes, alpha=1.0, fontsize=10)

    #from atlasify import atlasify
    #atlasify("Internal", indent=16)
    #plt.tight_layout(pad=1.08, h_pad=None, w_pad=None, rect=None)

    plt.savefig(outputName)

    plt.clf()

parser=argparse.ArgumentParser("Comparison of SUSY signals and DM")
parser.add_argument("-r","--region",help="Region to use")
args=parser.parse_args()

region=args.region
susy_sigs={}
dfmv_xsections={}

with open("pp13_stopsbottom_NNLO+NNLL.json","r") as f:
    susy_sigs=json.load(f)

with open("tcmet_signals_info_dmfv_vs_susy.json","r") as f:
    dmfv_xsections=json.load(f)

signals=[x.split("\n")[0].split(";") for x in open("my-signal-out.txt","r") if "mc15_13TeV" in x]

signals=[]
for bench,benchy in dmfv_xsections.items():
    for name,vals in benchy.items():
        signals.append(["_"+name,0,0,vals[2],0,vals[3]])

benchmarks={}
estimated={}
comparison={}


dmfv_uLs={}
susy_uLs=[]


for x in ["RHSFF","RHQDF","LHQDF1","LHQDF2"]:
    #with open("../../pyAna/statistics/tcMet/LimitPlots/MarawansJsons/DMFV_upperlimits_%s.json"%x,"r") as f:
    if not x in dmfv_uLs.keys():
        dmfv_uLs[x]=[]
    with open("../../pyAna/statistics/tcMet/LimitPlots/region%s_hypotest_%s_upperlimit__1_harvest_list.json"%(region,x),"r") as f:
        dmfv_uLs[x].extend(json.load(f))

#with open("../../../tcMeT/pyAna/statistics/tcMet/LimitPlots/SUSY_upperlimits.json","r") as f:
with open("../../../tcMeT/pyAna/statistics/tcMet/LimitPlots/region%s_hypotest_mass_upperlimit__1_harvest_list.json"%region,"r") as f:
    susy_uLs.extend(json.load(f))

for sig in signals:
    mass=int(sig[0].split("_m")[-1].split("_")[0])
    delta=float(sig[0].split("_d")[-1].split("_")[0].replace("p","."))
    sintheta=float(sig[0].split("_s")[-1].split("_")[0].replace("p","."))
    bench=sig[0].split("_s")[-1].split("_")[1]
    #xsecRef = float(sig[3])*1000.0 ## transform it into pb 
    xsecRef = float(sig[3])*0.001 ## transform it into pb 
    filterRef = float(sig[5])
    
    xsec_susy = susy_sigs["data"][str(mass)]["xsec_pb"]
    if not bench in benchmarks.keys():
        benchmarks[bench]={}
        estimated[bench]={}
        comparison[bench]={}

    dm_uL,susy_uL=None,None
    for tmpdict in dmfv_uLs[bench]:
        if tmpdict["mphi"] == mass and math.fabs(tmpdict["D_11"]-delta) < 0.00001:
            dm_uL=tmpdict["expectedUpperLimit"]
            break

    for tmpdict in susy_uLs:

        fallback=100
        mneut=100
        if bench == "LHQDF2":
            mneut=450
            fallback=400
        if bench == "RHQDF":
            mneut=200
            fallback=100
        if bench == "LHQDF1":
            mneut=200
            fallback=100
        if bench == "RHSFF":
            mneut=200
            fallback=150

        if tmpdict["mstop"] == mass and tmpdict["mneut"] == mneut :
            #susy_uL=tmpdict["expectedUpperLimit"]
            susy_uL=tmpdict["expectedUpperLimitmuSig"]
            break
        elif tmpdict["mstop"] == mass and tmpdict["mneut"] == fallback :
            #susy_uL=tmpdict["expectedUpperLimit"]
            susy_uL=tmpdict["expectedUpperLimitmuSig"]
            break 

    print(susy_uL, dm_uL)
    if xsec_susy > 0.00000001 and xsecRef > 0.0000001:
        benchmarks[bench][(mass,delta)]=xsecRef/xsec_susy
        if dm_uL and susy_uL:
            estimated[bench][(mass,delta)]=xsec_susy*susy_uL/xsecRef
            comparison[bench][(mass,delta)]=dm_uL - (xsec_susy*susy_uL/xsecRef)


for bench,points in benchmarks.items():
    print("\033[94mCross-section difference plot for benchmark %s and region %s\033[0m"%(bench,region))
    matplot(points,bench,region,"$\sigma_{DMFV}/\sigma_{SUSY}$",'plt_%s_%s.pdf'%(bench,region))

for bench,points in estimated.items():
    print("\033[94mEstimated plot for benchmark %s and region %s\033[0m"%(bench,region))
    if len(list(points.keys())) < 10:
        continue
    matplot(points,bench,region,"Estimated upper limit on $\mu_{DMFV}$",'est_%s_%s.pdf'%(bench,region))

for bench,points in comparison.items():
    if len(list(points.keys())) < 10:
        continue
    print("\033[94mComparison plot for benchmark %s and region %s\033[0m"%(bench,region))
    matplot(points,bench,region,"$\mu^{HF}_{DMFV} - \mu^{estimated}_{DMFV}$",'comp_%s_%s.pdf'%(bench,region))

