import os, sys
import os.path, subprocess
from os import listdir
from os.path import isfile, join
import fnmatch
from subprocess import Popen, PIPE
from math import sqrt
from array import array
import numpy as np
from optparse import OptionParser


def main():
    parser = OptionParser()
    parser.add_option("--LOGdir", dest="logdir", help="Directory containing the generation logs")
    (config, sys.argv[1:]) = parser.parse_args(sys.argv[1:])

    path = config.logdir #path with generation logs
    listdir = os.listdir(path)
    for d in listdir:
        if not ".log" in d: continue
        print(d)
        os.chdir(path+d)
        tarlist=os.listdir(path+d)
        for tar in tarlist:
            if "tarball" in tar: continue
            print tar
            proc = subprocess.call("tar xzf "+tar ,shell=True)
            




if __name__ == "__main__": # really important otherwise it does not call automatically main
    main()
