import ROOT,os

ROOT.xAOD.Init().ignore()


class SimpleAnalysis(ROOT.EL.AnaAlgorithm):

def runJob():
    sh = ROOT.SH.SampleHandler()
    sh.setMetaString( 'nc_tree', 'CollectionTree' )
    inputFilePath = os.getenv( 'ALRB_TutorialData' ) + '/mc21_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.deriv.DAOD_PHYS.e8357_s3802_r13508_p5057/'
    ROOT.SH.ScanDir().filePattern( 'DAOD_PHYS.28625583._000007.pool.root.1' ).scan( sh, inputFilePath )
    sh.printContent()

    # Create an EventLoop job.
    job = ROOT.EL.Job()
    job.sampleHandler( sh )
    job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )
    job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

    # Create the algorithm's configuration.
    from AnaAlgorithm.DualUseConfig import createAlgorithm
    alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )
    
    # later on we'll add some configuration options for our algorithm that go here
    
    # Add our algorithm to the job
    job.algsAdd( alg )
    
    # Run the job using the direct driver.
    driver = ROOT.EL.DirectDriver()
    driver.submit( job, options.submission_dir )
    
    ROOT.xAOD.TruthParticleContainer
