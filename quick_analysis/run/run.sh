#!/bin/bash

signals=( m700_d1p0_s0p25_RHSFF m700_d2p0_s0p2_LHQDF2 m700_d0p25_s0p2_LHQDF2 m700_d1p75_s0p2_LHQDF2 m700_d1p5_s0p2_LHQDF2 m700_d1p25_s0p2_LHQDF2 m700_d1p0_s0p2_LHQDF2 m700_d1p0_s0p1_LHQDF1 m700_d0p75_s0p2_LHQDF2 m700_d0p5_s0p2_LHQDF2 m700_d0p75_s0p1_LHQDF1 m700_d1p0_s0p2_RHQDF m700_d0p75_s0p25_RHSFF m700_d0p5_s0p1_LHQDF1 m700_d0p75_s0p2_RHQDF m700_d0p5_s0p25_RHSFF m700_d0p5_s0p2_RHQDF m700_d0p25_s0p2_RHQDF m700_d0p0_s0p2_LHQDF2 m700_d0p0_s0p25_RHSFF m700_d0p0_s0p1_LHQDF1 m700_d0p25_s0p1_LHQDF1 m700_d0p0_s0p2_RHQDF m700_d0p25_s0p25_RHSFF )

signals=( m800_d0p25_s0p25_RHSFF m800_d0p5_s0p25_RHSFF m800_d0p75_s0p25_RHSFF m800_d1p0_s0p25_RHSFF m800_d1p25_s0p25_RHSFF m800_d1p5_s0p25_RHSFF m800_d1p75_s0p25_RHSFF m800_d2p0_s0p25_RHSFF m500_d0p25_s0p25_RHSFF m500_d0p5_s0p25_RHSFF m500_d0p75_s0p25_RHSFF m500_d1p0_s0p25_RHSFF m500_d1p25_s0p25_RHSFF m500_d1p5_s0p25_RHSFF m500_d1p75_s0p25_RHSFF m500_d2p0_s0p25_RHSFF  m800_d0p25_s0p1_LHQDF1 m800_d0p5_s0p1_LHQDF1 m800_d0p75_s0p1_LHQDF1 m800_d1p0_s0p1_LHQDF1 m800_d1p25_s0p1_LHQDF1 m800_d1p5_s0p1_LHQDF1 m800_d1p75_s0p1_LHQDF1 m800_d2p0_s0p1_LHQDF1 m500_d0p25_s0p1_LHQDF1 m500_d0p5_s0p1_LHQDF1 m500_d0p75_s0p1_LHQDF1 m500_d1p0_s0p1_LHQDF1 m500_d1p25_s0p1_LHQDF1 m500_d1p5_s0p1_LHQDF1 m500_d1p75_s0p1_LHQDF1 m500_d2p0_s0p1_LHQDF1  m800_d0p25_s0p2_LHQDF2 m800_d0p5_s0p2_LHQDF2 m800_d0p75_s0p2_LHQDF2 m800_d1p0_s0p2_LHQDF2 m800_d1p25_s0p2_LHQDF2 m800_d1p5_s0p2_LHQDF2 m800_d1p75_s0p2_LHQDF2 m800_d2p0_s0p2_LHQDF2 m500_d0p25_s0p2_LHQDF2 m500_d0p5_s0p2_LHQDF2 m500_d0p75_s0p2_LHQDF2 m500_d1p0_s0p2_LHQDF2 m500_d1p25_s0p2_LHQDF2 m500_d1p5_s0p2_LHQDF2 m500_d1p75_s0p2_LHQDF2 m500_d2p0_s0p2_LHQDF2 m800_d0p25_s0p2_RHQDF m800_d0p5_s0p2_RHQDF m800_d0p75_s0p2_RHQDF m800_d1p0_s0p2_RHQDF m800_d1p25_s0p2_RHQDF m800_d1p5_s0p2_RHQDF m800_d1p75_s0p2_RHQDF m800_d2p0_s0p2_RHQDF m500_d0p25_s0p2_RHQDF m500_d0p5_s0p2_RHQDF m500_d0p75_s0p2_RHQDF m500_d1p0_s0p2_RHQDF m500_d1p25_s0p2_RHQDF m500_d1p5_s0p2_RHQDF m500_d1p75_s0p2_RHQDF m500_d2p0_s0p2_RHQDF )
#signals=(m800_d1p5_s0p1_LHQDF1)


signals=( m1200_d0p25_s0p25_RHSFF m1200_d1p0_s0p1_LHQDF1 m1200_d1p0_s0p25_RHSFF m1200_d1p0_s0p2_LHQDF2 m1200_d1p0_s0p2_RHQDF m1200_d2p0_s0p1_LHQDF1 m1200_d2p0_s0p25_RHSFF m1200_d2p0_s0p2_LHQDF2 m1200_d2p0_s0p2_RHQDF m600_d0p25_s0p25_RHSFF m600_d0p25_s0p2_LHQDF2 m600_d1p0_s0p1_LHQDF1 m600_d1p0_s0p25_RHSFF m600_d1p0_s0p2_LHQDF2 m600_d1p0_s0p2_RHQDF m600_d2p0_s0p1_LHQDF1 m600_d2p0_s0p25_RHSFF m600_d2p0_s0p2_LHQDF2 m600_d2p0_s0p2_RHQDF m800_d0p25_s0p25_RHSFF m800_d1p0_s0p1_LHQDF1 m800_d1p0_s0p25_RHSFF m800_d1p0_s0p2_LHQDF2 m800_d1p0_s0p2_RHQDF m800_d2p0_s0p1_LHQDF1 m800_d2p0_s0p25_RHSFF m800_d2p0_s0p2_LHQDF2 m800_d2p0_s0p2_RHQDF m900_d1p5_s0p2_RHQDF )
for sig in ${signals[*]};
do
    #echo "Prearing DAOD_TRUTH1.mc.MGPy8EG_A14N30NLO_${sig}_1LorMET.root signal samples"
    #run_local.py -i /lustre/fs22/group/atlas/alopezso/tcMeT/dmfv_truth_madspin/DAOD_TRUTH1.mc.MGPy8EG_A14N30NLO_${sig}_1LorMET.root -s madspin_$sig
    #run_local.py -i /lustre/fs22/group/atlas/alopezso/tcMeT/dmfv_truth/DAOD_TRUTH1.mc.MGPy8EG_A14N30NLO_${sig}_1LorMET.root -s no_madspin_$sig    
    #python madgraph_madspin_decays_comparison.py -s $sig -o plots_comparisons
    #if [[ $sig != *LHQDF* ]];
    #then
    #continue
    #fi

    echo "Prearing DAOD_TRUTH1.mc.MGPy8EG_A14N30NLO_${sig}_1LorMET.root signal samples"
    python /afs/ifh.de/group/atlas/users/alopezso/DMFV/simulateDMFV/quick_analysis/source/QuickAnalysis/python/run_local.py -i /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/Truthfiles/DAOD_TRUTH1.mc.MGPy8EG_A14_${sig}_1LorMET_nom.root -s mgdecays_$sig
    #run_local.py -i /lustre/fs22/group/atlas/alopezso/tcMeT/dmfv_truth_mgdecays/DAOD_TRUTH1.mc.MGPy8EG_A14N30NLO_${sig}_1LorMET.root -s mgdecays_$sgi
    #run_local.py -i /lustre/fs22/group/atlas/alopezso/tcMeT/dmfv_truth/DAOD_TRUTH1.mc.MGPy8EG_A14N30NLO_${sig}_1LorMET.root -s pythiadecays_$sig    
    #python pythia_vs_mg_decays_comparison.py -s $sig -o plots_comparisons
done
