import os,ROOT,argparse,glob,math



def get_brs_logfile(infile):

    print("Reading file %s"%infile)
    physicsProcess=None
    xsection=None
    filtereff=None
    width,time=None,None
    width_dw,time_dw=None,None
    brU,brD,brC,brS,brB,brT,brUadd,brTadd = None,None,None,None,None,None,None,None
    for inline in open(infile,"r"):
        inline=inline.split("\n")[0]
        if "physicsShort less than 50 characters" in inline:
            physicsProcess=inline.split("physicsShort")[0].split("OK: ")[-1].rstrip()
        if "Cross-section" in inline:
            xsection=float(inline.split("+-")[0].split("Cross-section :   ")[-1].rstrip())*1000 ## transform it into fb                                                                                   
        if "MetaData: GenFiltEff =" in inline:
            filtereff=float(inline.split("+-")[0].split("MetaData: GenFiltEff =")[-1].rstrip()) ## transform it into pb                                                                                   

        if "_RH" in infile:
            if "phi              phi~               2   2   1" in inline:
                allitems = [x.rstrip() for x in inline.split("phi              phi~               2   2   1 ")[-1].split(" ") if x.rstrip() !='']
                width=float(allitems[1])
                time=float(allitems[4])
        elif "_LH" in infile:
            if "phiup            phiup~             1   2   1" in inline:
                allitems = [x.rstrip() for x in inline.split("phiup            phiup~             1   2   1 ")[-1].split(" ") if x.rstrip() !='']
                width=float(allitems[1])
                time=float(allitems[4])
            if "phidown          phidown~           1  -1   1" in inline:
                allitems = [x.rstrip() for x in inline.split("phidown          phidown~           1  -1   1 ")[-1].split(" ") if x.rstrip() !='']
                width_dw=float(allitems[1])
                time_dw=float(allitems[4])

        if "100        4 -9900023" in inline:
            brC=float(inline.split("  100        4 -9900023")[0].split(" 1 ")[-1].rstrip())
        if "100        2 -9900022" in inline:
            brU=float(inline.split("  100        2 -9900022")[0].split(" 1 ")[-1].rstrip())
        if "100        5 -9900025" in inline:
            brB=float(inline.split("  100        5 -9900025")[0].split(" 1 ")[-1].rstrip())
        if "100        3 -9900023" in inline:
            brS=float(inline.split("  100        3 -9900023")[0].split(" 1 ")[-1].rstrip())
        if "100        1 -9900022" in inline:
            brD=float(inline.split("  100        1 -9900022")[0].split(" 1 ")[-1].rstrip())
        if "100        6 -9900025" in inline:
            brT=float(inline.split("  100        6 -9900025")[0].split(" 1 ")[-1].rstrip())


        if "100        2 -9900025" in inline:
            brUadd=float(inline.split("100        2 -9900025")[0].split(" 1 ")[-1].rstrip().lstrip())
        if "100        6 -9900022" in inline:
            brTadd=float(inline.split("100        6 -9900022")[0].split(" 1 ")[-1].rstrip().lstrip())

        if xsection and width and physicsProcess and filtereff and brC and brT and brU and brUadd and brTadd and (( "LHQSF" in infile and brD and brS and brB) or ("_RH" in infile)):
            break

    if not brUadd:
        brUadd = 0.0

    if not brTadd:
        brTadd = 0.0

    if not brU or not brC or not brT:
        return -1,-1,-1
    return brU+brUadd,brC,brT+brTadd
#return brU,brD,brC,brS,brB,brT

#parser=argparse.ArgumentParser("Getting the branching ratio")
#parser.add_argument("-f"."--filename",help="file to run on")
#parser.add_argument("-t","--tree",default="analysis",help="tree name")
#args=parser.parse_args()

all_files = glob.glob("mgdecays_m*-2024-09-20*/data-ANALYSIS/Truthfiles.root")
#all_files = glob.glob("mgdecays_m*-2024-09-16-18*/data-ANALYSIS/Truthfiles.root")
treename = "analysis"

for filename in all_files:
    name=filename.split("-2024")[0].split("decays_")[-1]

    brU,brC,brT = get_brs_logfile(f"/lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/uncertainties_production/{name}_nom/log.generate")

    if brU < 0 or brC < 0 or brT < 0:
        continue

    br_total = brU+brC+brT

    brU = brU/br_total ## needed for LHQDF
    brC = brC/br_total ## needed for LHQDF
    brT = brT/br_total ## needed for LHQDF
    
    rdf=ROOT.RDataFrame(treename,filename)
    totaL_events = rdf.Count().GetValue()
    tdmet = rdf.Filter("decay_type == 8  || decay_type == 1").Count().GetValue()/totaL_events
    tumet = rdf.Filter("decay_type == 9  || decay_type == 2").Count().GetValue()/totaL_events
    tsmet = rdf.Filter("decay_type == 10 || decay_type == 3").Count().GetValue()/totaL_events
    tcmet = rdf.Filter("decay_type == 11 || decay_type == 4").Count().GetValue()/totaL_events
    tbmet = rdf.Filter("decay_type == 12 || decay_type == 5").Count().GetValue()/totaL_events
    ttmet = rdf.Filter("decay_type == 13 || decay_type == 6").Count().GetValue()/totaL_events
    
    
    #print(f"Sample {name} has brs tdmet {tdmet:.2f}, tumet {tumet:.2f}, tsmet {tsmet:.2f}, tcmet {tcmet:.2f}, tbmet {tbmet:.2f}, ttmet {ttmet:.2f}")

    comp_brU = tumet/brU
    comp_brC = tcmet/brC
    comp_brT = ttmet/brT

    if math.fabs(comp_brU-1) > 0.1:
        print(f"\033[91mProblem in {name} as brU doesn' behave as expected: {tumet:.2f} (truth) vs {brU:.2f}(theory)\033[0m")
    elif math.fabs(comp_brC-1) > 0.1:
        print(f"\033[91mProblem in {name} as brC doesn' behave as expected: {tcmet:.2f} (truth) vs {brC:.2f}(theory)\033[0m")
    elif math.fabs(comp_brT-1) > 0.1:
        print(f"\033[91mProblem in {name} as brT doesn' behave as expected: {ttmet:.2f} (truth) vs {brT:.2f}(theory)\033[0m")
    else:
        print(f"\033[94mSample {name} behaves as expected \033[0m")

