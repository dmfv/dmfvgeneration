import ROOT,os,math,argparse
ROOT.gROOT.SetBatch(True)


parser=argparse.ArgumentParser("Plotting script of differences between the usage of Pythia or not")
parser.add_argument("-s","--signal",help="Signal name")
parser.add_argument("-o","--outputFolder",default="plots",help="Output folder")
args=parser.parse_args()

file_one = ROOT.TFile("mgdecays_%s/data-ANALYSIS/dmfv_truth_mgdecays.root"%args.signal,"READ")
file_two = ROOT.TFile("pythiadecays_%s/data-ANALYSIS/dmfv_truth.root"%args.signal,"READ")


os.system("mkdir -p %s"%args.outputFolder)

## Get a tree

tree_from_file_one = file_one.Get("analysis")
tree_from_file_two = file_two.Get("analysis")


## Reading from tree one 

histograms={
    "DeltaEtaDMMed":[ ROOT.TH1D("mad_d_eta_med_dm",";#Delta#eta(#phi,#chi);Generated events",30,-3,3), ROOT.TH1D("nomad_d_eta_med_dm",";#Delta#eta(#phi,#chi);Generated events",30,-3,3)],
    "DeltaEtaQuarkMed":[ ROOT.TH1D("mad_d_eta_med_quark",";#Delta#eta(#phi,q);Generated events",30,-3,3), ROOT.TH1D("nomad_d_eta_med_quark",";#Delta#eta(#phi,q);Generated events",30,-3,3)],
    "DeltaEtaQuarkDM":[ ROOT.TH1D("mad_d_eta_quark_dm",";#Delta#eta(q,#chi);Generated events",30,-3,3), ROOT.TH1D("nomad_d_eta_quark_dm",";#Delta#eta(q,#chi);Generated events",30,-3,3)],
    "DeltaPhiDMMed":[ ROOT.TH1D("mad_d_phi_med_dm",";#Delta#phi(#phi,#chi);Generated events",30,-3.14,3.14), ROOT.TH1D("nomad_d_phi_med_dm",";#Delta#phi(#phi,#chi);Generated events",30,-3.14,3.14)],
    "DeltaPhiQuarkMed":[ ROOT.TH1D("mad_d_phi_med_quark",";#Delta#phi(#phi,q);Generated events",30,-3.14,3.14), ROOT.TH1D("nomad_d_phi_med_quark",";#Delta#phi(#phi,q);Generated events",30,-3.14,3.14)],
    "DeltaPhiQuarkDM":[ ROOT.TH1D("mad_d_phi_quark_dm",";#Delta#phi(q,#chi);Generated events",30,-3.14,3.14), ROOT.TH1D("nomad_d_phi_quark_dm",";#Delta#phi(q,#chi);Generated events",30,-3.14,3.14)],
    "decay_type":[ ROOT.TH1D("mad_decay_type",";Decay type;Generated events",15,-1,14), ROOT.TH1D("nomad_decay_type",";Decay type;Generated events",15,-1,14)],
    "production_type":[ ROOT.TH1D("mad_production_type",";Production type;Generated events",15,-1,14), ROOT.TH1D("nomad_production_type",";Production type;Generated events",15,-1,14)],
    "nLep":[ ROOT.TH1D("mad_nLep",";Number of leptons;Generated events",10,-1,9), ROOT.TH1D("nomad_nLep",";Number of leptons ;Generated events",10,-1,9)],
    "nEl":[ ROOT.TH1D("mad_nEl",";Number of electrons;Generated events",10,-1,9), ROOT.TH1D("nomad_nEl",";Number of electrons ;Generated events",10,-1,9)],
    "nMu":[ ROOT.TH1D("mad_nMu",";Number of muons;Generated events",10,-1,9), ROOT.TH1D("nomad_nMu",";Number of muons ;Generated events",10,-1,9)],
    "nTop":[ ROOT.TH1D("mad_nTop",";Number of tops;Generated events",10,-1,9), ROOT.TH1D("nomad_nTop",";Number of tops ;Generated events",10,-1,9)],
    "nDM":[ ROOT.TH1D("mad_nDM",";Number of DM particles;Generated events",10,-1,9), ROOT.TH1D("nomad_nDM",";Number of DM particles;Generated events",10,-1,9)],
    "nMed":[ ROOT.TH1D("mad_nMed",";Number of mediators;Generated events",10,-1,9), ROOT.TH1D("nomad_nMed",";Number of mediators;Generated events",10,-1,9)],
}


mad_histogram2d = ROOT.TH2D("mad_noquark",";First Particle PdgId; Second Particle PdgId", 30,0,30,30,0,30)
nomad_histogram2d = ROOT.TH2D("nomad_noquark",";First Particle PdgId; Second Particle PdgId", 30,0,30,30,0,30)

mad_mother2d = ROOT.TH2D("mad_mother",";First Particle PdgId; Second Particle PdgId", 30,0,30,30,0,30)
nomad_mother2d = ROOT.TH2D("nomad_mother",";First Particle PdgId; Second Particle PdgId", 30,0,30,30,0,30)


### Loop over all the events of the tree
for event in tree_from_file_one:
    #if event.decay_type != 4:
    #    continue
    #if (getattr(event,'nMed') != 2):
    #    continue

    for var,histogram in histograms.items():
        var_value=getattr(event,var)
        histogram[0].Fill(var_value)
    vector_noquark = getattr(event,"NoQuark")
    vector_mother = getattr(event,"Mothers")
    if(vector_noquark.size() > 1):
        mad_histogram2d.Fill(math.fabs(vector_noquark[0]),math.fabs(vector_noquark[1]))
    if(vector_mother.size() >1):
        mad_mother2d.Fill(math.fabs(vector_mother[0]),math.fabs(vector_mother[1]))

for event in tree_from_file_two:
    #if event.decay_type != 4:
    #    continue
    if (getattr(event,'nMed') != 2):
        continue

    for var,histogram in histograms.items():
        var_value=getattr(event,var)
        histogram[1].Fill(var_value)
    vector_noquark = getattr(event,"NoQuark")
    vector_mother = getattr(event,"Mothers")
    if(vector_noquark.size() > 1):
        nomad_histogram2d.Fill(math.fabs(vector_noquark[0]),math.fabs(vector_noquark[1]))
    if(vector_mother.size() >1):
        nomad_mother2d.Fill(math.fabs(vector_mother[0]),math.fabs(vector_mother[1]))


# Plotting the histograms

for var,histogram in histograms.items():

    canvas=ROOT.TCanvas("canvas","canvas",1000,1000)
    canvas.cd()

    histogram[0].SetLineColor(ROOT.kBlue) ## Setting a color for the line of the histogram
    histogram[0].SetMarkerColor(ROOT.kBlue) ## Setting a color for the marker of the histogram
    histogram[0].SetLineWidth(2) ## Make a line with a coarser width
    histogram[0].SetMarkerStyle(ROOT.kFullCircle) ## Making sure the marker is a full circle. 
    
    histogram[1].SetLineColor(ROOT.kRed) ## Setting a color for the line of the histogram
    histogram[1].SetMarkerColor(ROOT.kRed) ## Setting a color for the marker of the histogram
    histogram[1].SetLineWidth(2) ## Make a line with a coarser width
    histogram[1].SetMarkerStyle(ROOT.kFullCircle) ## Making sure the marker is a full circle. 

    histogram[0].SetStats(0) ## Remove the annoying summary that always appears on the top-right corner 

    canvas.cd()
    histogram[0].SetMaximum(max(histogram[0].GetMaximum(),histogram[1].GetMaximum())*1.5)
    histogram[0].Draw("histE") ## will draw the first histogram as a "hist", or just one line instead of markers
    histogram[1].Draw("histE""SAME") ## will draw the second in the same way. the "SAME" states that it has to be drawn on top of the first histogram and not substitute it in the canvas 
    #histogram[0].DrawNormalized("histE") ## will draw the first histogram as a "hist", or just one line instead of markers
    #histogram[1].DrawNormalized("histE""SAME") ## will draw the second in the same way. the "SAME" states that it has to be drawn on top of the first histogram and not substitute it in the canvas 
    

    ## Create a TLegend
    
    legend_x_min = 0.15 
    legend_x_max = 0.45 
    legend_y_min = 0.7 
    legend_y_max = 0.85

    legend = ROOT.TLegend(legend_x_min,legend_y_min,legend_x_max,legend_y_max) ## TLegend(x1,y1,x2,y2). x1, x2, y1, y2 correspond to relative positions in the canvas (x1 = 0.6 ==> low_x_legend  = xmin+0.6*(xmax-xmin)  etc)
    legend.SetBorderSize(0)
    legend.SetTextSize(0.04)
    legend.SetTextFont(42) ## to be checked in TAttText
    legend.SetHeader("#bf{#it{ATLAS}} Internal")
    legend.AddEntry(histogram[0],"Madgraph decays","l") ## Add the information for each histogram you plot
    legend.AddEntry(histogram[1],"Pythia decays","l") ## Add the information for each histogram you plot
    ### draw the legend
    legend.Draw("SAME")
    latex=ROOT.TLatex()
    latex.SetTextFont(42)
    latex.SetTextSize(0.02)
    params=args.signal.split("_")
    latex.DrawLatexNDC(0.65,0.8,"m_{#phi} = %s GeV, D_{#lambda,11} = %s, \\ sin#theta = %s,  %s "%(params[0].replace("m",""),
                                                                               params[1].replace("d","").replace("p","."),
                                                                               params[2].replace("s","").replace("p","."),
                                                                               params[3] ))
    ## Save the cavnas under a name
    canvas.SaveAs(args.outputFolder+"/"+var+"_"+args.signal+".pdf","RECREATE")
    canvas.Delete()

canvas=ROOT.TCanvas("canvas","canvas",1000,1000)
canvas.cd()
mad_histogram2d.Draw("COLZ")
canvas.SaveAs(args.outputFolder+"/noquark_mad_"+args.signal+".pdf","RECREATE")
nomad_histogram2d.Draw("COLZ")
canvas.SaveAs(args.outputFolder+"/noquark_nomad_"+args.signal+".pdf","RECREATE")

mad_mother2d.Draw("COLZ")
canvas.SaveAs(args.outputFolder+"/mother_mad_"+args.signal+".pdf","RECREATE")
nomad_mother2d.Draw("COLZ")
canvas.SaveAs(args.outputFolder+"/mother_nomad_"+args.signal+".pdf","RECREATE")

file_one.Close()
file_two.Close()
