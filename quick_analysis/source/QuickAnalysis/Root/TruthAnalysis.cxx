#include <AsgMessaging/MessageCheck.h>
#include <QuickAnalysis/TruthAnalysis.h>
#include "MCTruthClassifier/MCTruthClassifierDefs.h"


using namespace MCTruthPartClassifier;

TruthAnalysis :: TruthAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}

TruthAnalysis :: ~TruthAnalysis(){
  /*delete m_jetEta;
  delete m_jetPhi;
  delete m_jetPt;
  delete m_jetE;
  */
}

StatusCode TruthAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  // new code to retrive the xStream from the fileMetaData                                                                                                                                                  
  ANA_CHECK (book (TTree ("analysis", "Truth analysis ntuple")));
  TTree* mytree = tree ("analysis");
  mytree->Branch ("RunNumber", &m_runNumber);
  mytree->Branch ("EventNumber", &m_eventNumber);
  mytree->Branch ("DeltaPhiQuarkMed", &m_dphi_quark_med);
  mytree->Branch ("DeltaPhiDMMed", &m_dphi_dm_med);
  mytree->Branch ("DeltaPhiQuarkDM", &m_dphi_quark_dm);
  mytree->Branch ("DeltaEtaQuarkMed", &m_deta_quark_med);
  mytree->Branch ("DeltaEtaDMMed", &m_deta_dm_med);
  mytree->Branch ("DeltaEtaQuarkDM", &m_deta_quark_dm);
  mytree->Branch ("production_type", &m_production_type);
  mytree->Branch ("decay_type", &m_decay_type);
  mytree->Branch ("nEl", &m_nEl);
  mytree->Branch ("nMu", &m_nMu);
  mytree->Branch ("nLep", &m_nLep);
  mytree->Branch ("nTop", &m_nTop);
  mytree->Branch ("nDM", &m_nDM);
  mytree->Branch ("nMed", &m_nMed);
  mytree->Branch ("Met_Vis", &m_metInt);
  mytree->Branch ("Met_Inv", &m_metNonInt);

  m_noquark=new std::vector<int>();
  m_mothers=new std::vector<int>();

  mytree->Branch ("NoQuark", &m_noquark);  
  mytree->Branch ("Mothers", &m_mothers);  
  /*m_jetEta = new std::vector<float>();
  mytree->Branch ("JetEta", &m_jetEta);
  m_jetPhi = new std::vector<float>();
  mytree->Branch ("JetPhi", &m_jetPhi);
  m_jetPt = new std::vector<float>();
  mytree->Branch ("JetPt", &m_jetPt);
  m_jetE = new std::vector<float>();
  mytree->Branch ("JetE", &m_jetE);
  */
  m_event=0;
  m_truthClassifier = new MCTruthClassifier("MonteCarloTruthClassifier");
  m_truthClassifier->initialize();
  return StatusCode::SUCCESS;
}



StatusCode TruthAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  m_event+=1;
  if(m_event%1000 == 0)
    std::cout << " Event number : " << m_event << std::endl;


  m_noquark->clear();
  m_mothers->clear();

  m_runNumber = 0;
  m_eventNumber = 0;
  m_dphi_quark_med=-999.0;
  m_dphi_dm_med=-999.0;
  m_dphi_quark_dm=-999.0;
  m_deta_quark_med=-999.0;
  m_deta_dm_med=-999.0;
  m_deta_quark_dm=-999.0;
  m_decay_type=-1;
  m_production_type=-1;
  m_nEl=0;
  m_nMu=0;
  m_nLep=0;
  m_nTop=0;
  m_nDM=0;
  m_nMed=0;
  m_metInt=0.0;
  m_metNonInt=0.0;

  event = wk()->xaodEvent();
  //  const xAOD::FileMetaData* fmd = nullptr;
  //ANA_CHECK(event->retrieveMetaInput(fmd, "MetaData"));
  m_stream=wk()->inputFile()->GetName();//TString(fmd->auxdata<std::string>("dataType"));

  //  std::cout << "Stream is : " << m_stream << std::endl;
  const xAOD::EventInfo* event_info = 0;
  ANA_CHECK(evtStore()->retrieve(event_info,"EventInfo") );

  const xAOD::TruthParticleContainer* truth_bsm = 0;

  if(m_stream.Contains("TRUTH3")){
    ANA_CHECK(evtStore()->retrieve(truth_bsm,"TruthBSMWithDecayParticles"));
  }else if(m_stream.Contains("TRUTH1")){
    ANA_CHECK(evtStore()->retrieve(truth_bsm,"TruthParticles") );
  }else if(m_stream.Contains("TRUTH0")){
    ANA_CHECK(evtStore()->retrieve(truth_bsm,"TruthParticles") );
  }else{
    std::cout << " I cannot work with Stream " << m_stream << std::endl;
    return StatusCode::FAILURE;
  }
    
  const xAOD::TruthParticleContainer* truth_electrons = 0;
  ANA_CHECK(evtStore()->retrieve(truth_electrons,"TruthElectrons") );

  const xAOD::TruthParticleContainer* truth_muons = 0;
  ANA_CHECK(evtStore()->retrieve(truth_muons,"TruthMuons") );


  const xAOD::TruthParticleContainer* truth_top = 0;
  ANA_CHECK(evtStore()->retrieve(truth_top,"TruthTop") );

  const xAOD::MissingETContainer* truth_met = 0;
  ANA_CHECK(evtStore()->retrieve(truth_met,"MET_Truth") );


  std::vector<const xAOD::TruthParticle*> mediators;
  std::vector<const xAOD::TruthParticle*> darkmatter;
  std::vector<const xAOD::TruthParticle*> topquarks;


  for(const xAOD::TruthParticle* med: *truth_bsm){
    //if (fabs(med->status() ) == 1 ){
    //std::cout << "Particle with pdgId " << med->pdgId() << std::endl;
    //}
    /*
    if ( (fabs(med->pdgId()) == 53 || fabs(med->pdgId()) == 54)){
      std::cout << "      Med is found!  Id: " << med->pdgId() << " status : "<<  med->status() << std::endl;
      std::cout << "Parents are : " ;
      for(unsigned  int iparent = 0 ; iparent < med->nParents() ; ++iparent){
	std::cout << med->parent(iparent)->pdgId() << " | " ;
      }
      std::cout << std::endl;

      std::cout << "Children are : " ;
      for(unsigned  int ichild = 0 ; ichild < med->nChildren() ; ++ichild){
	std::cout << med->child(ichild)->pdgId() << " | " ;
      }
      std::cout << std::endl;
      }
    */
    // Mediators 
    //if( (fabs(med->pdgId()) == 53 || fabs(med->pdgId()) == 54) && (med->status() == 22 || med->status() == 62) ){
    if( (fabs(med->pdgId()) == 53 || fabs(med->pdgId()) == 54) && (med->status() == 62) ){
      //for (unsigned  int iparent = 0 ; iparent < med->nParents() ; ++iparent){
      //  const xAOD::TruthParticle* parent = med->parent(iparent);
        //std::cout << "      Parent is found!  Id: " << parent->pdgId() << " status : "<<  med->status() << std::endl;
      //}
      //if(med->nChildren() == 0 ){
	//std::cout << " ******* No children found here ******** " << std::endl;
      //continue;
      //}     
      // std::cout << "Truth boson found ! pdgId " << med->pdgId()<< " status : " <<  med->status()<<" | number of parents :" << med->nParents()<< " | number of children :" << med->nChildren() << std::endl;

      mediators.push_back(med);

      //for (unsigned  int ichild = 0 ; ichild < med->nChildren() ; ++ichild){
      //  const xAOD::TruthParticle* child = med->child(ichild);
        //std::cout << "      Child is found!  Id: " << child->pdgId() << " status : "<<  med->status() << std::endl;	
      // }
    }
  }

  m_nMed = mediators.size();
  //std::cout << " ====== Number of mediators found : " << mediators.size() << " ============  " <<std::endl;  

  for(const xAOD::TruthParticle* dm: *truth_bsm){
    // DM particles
    /*
    if ( fabs(dm->pdgId()) > 990000){
      std::cout << "      DM is found!  Id: " << dm->pdgId() << " status : "<<  dm->status() << std::endl;
      std::cout << "Parents are : " ;
      for(unsigned  int iparent = 0 ; iparent < dm->nParents() ; ++iparent){
	std::cout << dm->parent(iparent)->pdgId() << " | " ;
      }
      std::cout << std::endl;

      std::cout << "Children are : " ;
      for(unsigned  int ichild = 0 ; ichild < dm->nChildren() ; ++ichild){
	std::cout << dm->child(ichild)->pdgId() << " | " ;
      }
      std::cout << std::endl;
    }
    */
    if( (fabs(dm->pdgId()) > 990000) && dm->status() == 1){
      darkmatter.push_back(dm);
    }
  }
  m_nDM=darkmatter.size();

  //  std::cout << " ====== Number of darkmatter found : " << darkmatter.size() << " ============  " <<std::endl;
  for(const xAOD::TruthParticle* top: *truth_top){
    // top particles
    if(!top)
      continue;
    if( (fabs(top->pdgId()) == 6) && top->status() == 22){
      topquarks.push_back(top);
    }
  }
  m_nTop = topquarks.size();
  //std::cout << " ====== Number of topquarks found : " << topquarks.size() << " ============  " <<std::endl;

  for(const xAOD::TruthParticle* electron: *truth_electrons){
    if(electron->status() != 1)
      continue;
    if(electron->nParents() != 1)
      continue;
    if(fabs(electron->parent(0)->pdgId()) != 24)
      continue;
    m_nEl+=1;
  }

  //std::cout << " ====== Number of electrons found : " << m_nEl << " ============  " <<std::endl;
  for(const xAOD::TruthParticle* muon: *truth_muons){
    if(muon->status() != 1)
      continue;
    if(muon->nParents() != 1)
      continue;
    if(fabs(muon->parent(0)->pdgId()) != 24 )
      continue;
    m_nMu+=1;
  }

  m_nLep = m_nEl+m_nMu;
  // std::cout << " ====== Number of muons found : " << m_nMu << " ============  " <<std::endl;


  // Production mode

  // There can only be one or two mediators and they need to be produced either by gluons (s-channel), radiation from a quark or from t-channel exchanging a dark matter. Take the first one then:
  m_production_type = -1;

  if(mediators.size() > 0 ){
    const xAOD::TruthParticle* parent = mediators[0];
    while(parent->nParents() == 1 && (fabs(parent->parent(0)->pdgId()) == 53|| fabs(parent->parent(0)->pdgId()) == 54)){
      parent=parent->parent(0);
    }
    if(parent->nParents() != 0 ){
      m_production_type = 0;
      if (fabs(parent->parent(0)->pdgId()) == 21 ){
	m_production_type=13;  // s-channel with gluons
      }else if(fabs(parent->parent(0)->pdgId()) == 1 && mediators.size() == 1){
	m_production_type=1;  // radiated from d-quark
      }else if(fabs(parent->parent(0)->pdgId()) == 2 && mediators.size() == 1){
	m_production_type=2;  // radiated from u-quark
      }else if(fabs(parent->parent(0)->pdgId()) == 3 && mediators.size() == 1){
	m_production_type=3;  // radiated from s-quark
      }else if(fabs(parent->parent(0)->pdgId()) == 4 && mediators.size() == 1){
	m_production_type=4;  // radiated from c-quark
      }else if(fabs(parent->parent(0)->pdgId()) == 5 && mediators.size() == 1){
	m_production_type=5;  // radiated from b-quark
      }else if(fabs(parent->parent(0)->pdgId()) == 6 && mediators.size() == 1){
	m_production_type=6;  // radiated from t-quark
      }else if(fabs(parent->parent(0)->pdgId()) == 1 && mediators.size() == 2){
	m_production_type=7;  // t-channel from d-quark
      }else if(fabs(parent->parent(0)->pdgId()) == 2 && mediators.size() == 2){
	m_production_type=8;  // t-channel from u-quark
      }else if(fabs(parent->parent(0)->pdgId()) == 3 && mediators.size() == 2){
	m_production_type=9;  // t-channel from s-quark
      }else if(fabs(parent->parent(0)->pdgId()) == 4 && mediators.size() == 2){
	m_production_type=10;  // t-channel from c-quark
      }else if(fabs(parent->parent(0)->pdgId()) == 5 && mediators.size() == 2){
	m_production_type=11;  // t-channel from b-quark
      }else if(fabs(parent->parent(0)->pdgId()) == 6 && mediators.size() == 2){
	m_production_type=12;  // t-channel from t-quark
      }   
      
    }    
  }

  

  // Decay the decay

  m_decay_type = 0;
  bool topisfound=false;

  // One of the mediators is required to decay into a top quark. Look at the other mediator.
  for(unsigned int imed = 0; imed < mediators.size() ; ++imed ){
    //    if( m_decay_type > 0 && m_decay_type != 6)
    //  continue;
    if (mediators[imed]->nChildren() != 2){
      std::cout << "Children are : " ; 
      for(unsigned  int ichild = 0 ; ichild < mediators[imed]->nChildren() ; ++ichild){
	std::cout << mediators[imed]->child(ichild)->pdgId() << " | " ;
      }                                                                                                                                                                                                    
      std::cout << std::endl; 
    }

    if (mediators[imed]->nChildren() !=2 )
      continue;       
       
    if(fabs(mediators[imed]->child(0)->pdgId()) < 990000){
      
      m_dphi_quark_med=TVector2::Phi_mpi_pi(mediators[imed]->child(0)->phi()-mediators[imed]->phi());
      m_dphi_dm_med=TVector2::Phi_mpi_pi(mediators[imed]->child(1)->phi()-mediators[imed]->phi());
      m_dphi_quark_dm=TVector2::Phi_mpi_pi(mediators[imed]->child(0)->phi()-mediators[imed]->child(1)->phi());
      
      m_deta_quark_med=mediators[imed]->child(0)->eta()-mediators[imed]->eta();
      m_deta_dm_med=mediators[imed]->child(1)->eta()-mediators[imed]->eta();
      m_deta_quark_dm=mediators[imed]->child(0)->eta()-mediators[imed]->child(1)->eta();

      if (fabs(mediators[imed]->child(0)->pdgId()) == 1 ){
	m_decay_type=1;  // d-quark + DM decay
      }else if(fabs(mediators[imed]->child(0)->pdgId()) == 2 ){
	m_decay_type=2;  // u-quark + DM decay
      }else if(fabs(mediators[imed]->child(0)->pdgId()) == 3  ){
	m_decay_type=3;  // s-quark + DM decay
      }else if(fabs(mediators[imed]->child(0)->pdgId()) == 4  ){
	m_decay_type=4; // c-quark + DM decay
      }else if(fabs(mediators[imed]->child(0)->pdgId()) == 5  ){
	m_decay_type=5; // b-quark + DM decay
      }else if(fabs(mediators[imed]->child(0)->pdgId()) == 6 and !topisfound ){
	topisfound=true; // top-quark + DM decay
	if(m_nMed == 1) // Case that there is only a radiation of one mediator
	  m_decay_type=6;
      }else if(fabs(mediators[imed]->child(0)->pdgId()) == 6 and topisfound ){
	m_decay_type=6;
      }   
    }else if(fabs(mediators[imed]->child(1)->pdgId()) < 990000){ // Look at the second child
      m_dphi_quark_med=TVector2::Phi_mpi_pi(mediators[imed]->child(1)->phi()-mediators[imed]->phi());
      m_dphi_dm_med=TVector2::Phi_mpi_pi(mediators[imed]->child(0)->phi()-mediators[imed]->phi());
      m_dphi_quark_dm=TVector2::Phi_mpi_pi(mediators[imed]->child(1)->phi()-mediators[imed]->child(0)->phi());
      
      m_deta_quark_med=mediators[imed]->child(1)->eta()-mediators[imed]->eta();
      m_deta_dm_med=mediators[imed]->child(0)->eta()-mediators[imed]->eta();
      m_deta_quark_dm=mediators[imed]->child(1)->eta()-mediators[imed]->child(0)->eta();
      if (fabs(mediators[imed]->child(1)->pdgId()) == 1 ){
	m_decay_type=1;  // d-quark + DM decay
      }else if(fabs(mediators[imed]->child(1)->pdgId()) == 2 ){
	m_decay_type=2;  // u-quark + DM decay
      }else if(fabs(mediators[imed]->child(1)->pdgId()) == 3  ){
	m_decay_type=3;  // s-quark + DM decay
      }else if(fabs(mediators[imed]->child(1)->pdgId()) == 4  ){
	m_decay_type=4; // c-quark + DM decay
      }else if(fabs(mediators[imed]->child(1)->pdgId()) == 5  ){
	m_decay_type=5; // b-quark + DM decay
      }else if(fabs(mediators[imed]->child(1)->pdgId()) == 6 and !topisfound ){
	topisfound=true; // top-quark + DM decay
	if(m_nMed == 1) // Case that there is only a radiation of one mediator
	  m_decay_type=6;
      }else if(fabs(mediators[imed]->child(1)->pdgId()) == 6 and topisfound ){
	m_decay_type=6;
      }   
    }else{
      std::cout << "Unrecognised decay " << std::endl;
    }
    if(mediators.size() == 1 )
      m_decay_type+=7; // Allocating from 7-13 for the case where only one mediator is available
    
  }
  
  
  
  /*
  for(const xAOD::TruthParticle* med: *truth_bsm){

    
    if(fabs(med->pdgId() == 6) && med->status() == 22 && med->nChildren() == 2){
      //std::cout << "Truth DM found ! pdgId " << med->pdgId()<< " pT: " << med->pt()/1000.0 << " GeV eta : " << med->eta() << " phi : " << med->phi() << " status : "<<  med->status()<<" number of children :" << med->nChildren() << std::endl;  
      m_nTop+=1;
    }
    if(fabs(med->pdgId() > 900000) && med->status() == 23){
      //std::cout << "Truth DM found ! pdgId " << med->pdgId()<< " pT: " << med->pt()/1000.0 << " GeV eta : " << med->eta() << " phi : " << med->phi() << " status : "<<  med->status()<<" number of children :" << med->nChildren() << std::endl;  
      m_nDM+=1;

    }
    if(fabs(med->pdgId()) == 53 || fabs(med->pdgId()) == 54){

      //std::cout << "Truth boson found ! pdgId " << med->pdgId()<< " pT: " << med->pt()/1000.0 << " GeV eta : " << med->eta() << " phi : " << med->phi() << " status : "<<  med->status()<<" number of children :" << med->nChildren() << std::endl;

      // Inspect all the children 
      for (unsigned  int ichild = 0 ; ichild < med->nChildren() ; ++ichild){
	const xAOD::TruthParticle* child = med->child(ichild);
	//std::cout << "      Children is found!  Id: " << child->pdgId() <<" pT: " << child->pt()/1000.0 << " GeV eta : " << child->eta() << " phi : " << child->phi() << " status : "<<  med->status()<<" number of children :" << child->nChildren() << std::endl;
	
      }

      if(med->status() == 22){
	for (unsigned  int iparent = 0 ; iparent < med->nParents() ; ++iparent){
	  const xAOD::TruthParticle* parent = med->parent(iparent);
	  if(parent)
	    m_mothers->push_back(parent->pdgId());
	}
      }

      // Just go for cases where real decays happen
      if(med->status() == 62 && med->nChildren()>1 ){
	m_nMed+=1;
	const xAOD::TruthParticle* quark = nullptr;
	const xAOD::TruthParticle* dm    = nullptr;

	for (unsigned  int ichild = 0 ; ichild < med->nChildren() ; ++ichild){
	  const xAOD::TruthParticle* child = med->child(ichild);
	  if( fabs(child->pdgId()) <=6 ){
	    quark = child;
	  }
	  if( fabs(child->pdgId()) > 900000 ){
	    dm = child;
	  }
	}

	if(!dm ||!quark){
	  for (unsigned  int ichild = 0 ; ichild < med->nChildren() ; ++ichild){
	    const xAOD::TruthParticle* child = med->child(ichild);
	    m_noquark->push_back(child->pdgId());
	  }
	  continue;
	}

	
	m_dphi_quark_med=TVector2::Phi_mpi_pi(quark->phi()-med->phi());
	m_dphi_dm_med=TVector2::Phi_mpi_pi(dm->phi()-med->phi());
	m_dphi_quark_dm=TVector2::Phi_mpi_pi(quark->phi()-dm->phi());

	m_deta_quark_med=quark->eta()-med->eta();
	m_deta_dm_med=dm->eta()-med->eta();
	m_deta_quark_dm=quark->eta()-dm->eta();

	// DEcay type for plotting purposes
	if (fabs(quark->pdgId()) == 1 ){
	  m_decay_type=1;  // d-quark + DM decay
	}else if(fabs(quark->pdgId()) == 2 ){
	  m_decay_type=2;  // u-quark + DM decay
	}else if(fabs(quark->pdgId()) == 3  ){
	  m_decay_type=3;  // s-quark + DM decay
	}else if(fabs(quark->pdgId()) == 4  ){
	  m_decay_type=4; // c-quark + DM decay
	}else if(fabs(quark->pdgId()) == 5  ){
	  m_decay_type=5; // b-quark + DM decay
	}else if(fabs(quark->pdgId()) == 6  ){
	  m_decay_type=6; // top-quark + DM decay
	}


      } // end of inspection of decays 
      

    }
  }

  */


  m_metInt=(*truth_met)["Int"]->met()/1000.0;
  m_metNonInt=(*truth_met)["NonInt"]->met()/1000.0;



  tree("analysis")->Fill();

  return StatusCode::SUCCESS;
}



StatusCode TruthAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}
