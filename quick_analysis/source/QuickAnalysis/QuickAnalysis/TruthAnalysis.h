#ifndef QuickAnalysis_TruthAnalysis_H
#define QuickAnalysis_TruthAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODMissingET/MissingET.h"
#include "xAODEventInfo/EventInfo.h"
#include "MCTruthClassifier/MCTruthClassifier.h"
#include "xAODMetaData/FileMetaData.h"
#include "TTree.h"

#include <EventLoop/Worker.h>


class TruthAnalysis : public EL::AnaAlgorithm
{
 public:
  // this is a standard algorithm constructor
  TruthAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
  ~TruthAnalysis();

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

 private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;
  unsigned int m_runNumber; //!
  unsigned long long m_eventNumber; //!
  float m_dphi_quark_med; //!
  float m_dphi_dm_med; //!
  float m_dphi_quark_dm; //!
  float m_deta_quark_med; //!
  float m_deta_dm_med; //!
  float m_deta_quark_dm; //!
  int m_decay_type;//!
  int m_production_type;//!
  int m_event;//!
  int m_nEl;//!
  int m_nMu;//!
  int m_nLep;//!
  int m_nTop;//!
  int m_nDM;//!
  int m_nMed;//!
  float m_metInt;//!
  float m_metNonInt;//!
  std::vector<int>* m_noquark;//!
  std::vector<int>* m_mothers;//!
  MCTruthClassifier* m_truthClassifier; //!
  TString m_stream;

  xAOD::TEvent* event; //!                                                                                                                                                                                  

};

#endif
