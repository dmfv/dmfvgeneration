list="dmfv_signals.txt"
for input in `cat $list`
do
    if [[ $input == *#* ]]
    then
	continue

    fi
    output=${input##*alopezso.}
    output=${output%%_EXT0*}
    pathena --trf "Reco_tf.py --inputEVNTFile=%IN --outputDAODFile=%OUT.pool.root --reductionConf=TRUTH3 --AMIConfig=p4395" --inDS=$input --outDS=user.alopezso.$output.TRUTH3

done
