import os,pandas

from lib.SinglePlot import *

def get_xsection(infile):

    for line in open(infile,'r'):
        if "Cross-section " in line and "pb" in line:
            return float( line.split(":")[-1].split("+-")[0].strip() )*1000.0

    return -1


def get_parameters(infile):
    params = infile.split("/")[-1].split("_")
    return {
        #'dsid': int(params[0]),
        'mMed': float(params[0].replace("m","")),
        'dll' : float(params[1].replace("d","").replace("p",".")),
        'sin' : float(params[2].replace("s","").replace("p",".")),
        'benchmark': params[3].split(".")[0],
    }

def get_Giacomos_parameters(line,struc):

    parameters = line.split(" ")
    params={}
    for i,s in enumerate(struc):
        params[s] = float(parameters[i])

    return params


list_of_files = []
for root,dirs,files in os.walk("inclusive"):
    list_of_files.extend([os.path.join(root,x) for x in files ])


plots = {}

for lfile in list_of_files:
    xsection = get_xsection(lfile)
    params = get_parameters(lfile)
    #if params["benchmark"] == "LHQSF2" and params["dll"] <= 0.4 and params["mMed"] == 500:
    #        continue
    if not (params["sin"],params["benchmark"]) in plots.keys():
        plots[(params["sin"],params["benchmark"])] = SinglePlot(params["sin"],params["benchmark"], "$m_{\phi} [GeV]$","$D_{\lambda,11}$","Cross-section [fb]")
    if xsection < 0.0:
        print("Problem in logfile %s. Cannot find cross-section. Omitting this result."%lfile)
        continue
    plots[(params["sin"],params["benchmark"])].append([params["mMed"] , params["dll"] , xsection] )

for iD,plot in plots.items():
    print("Plotting cross-sections for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("xsections_sintheta%s_%s.pdf"%( str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    plot.plot("xsections_sintheta%s_%s_log.pdf"%( str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)




list_of_files = []
for root,dirs,files in os.walk("GiacomosReference"):
    list_of_files.extend([os.path.join(root,x) for x in files if "phi" in x])

ref_plots = {}
div_plots = {}


structure = {
    "RHSFF" : ["mMed","mdm","dll","dlltwo","dllthree","brd","brc","brt","wphi","xsection"],
    "RHQDF" : ["mMed","mdm","dll","dlltwo","dllthree","brd","brc","brt","wphi","xsection"],
    "LHQDF1" : ["mMed","mdm","dll","dllthree","bru","brd","brt","brb","wphiup","wphidown","xsection"],
    "LHQDF2" : ["mMed","mdm","dll","dllthree","bru","brd","brt","brb","wphiup","wphidown","xsection"],
}

for lfile in list_of_files:
    benchmark=lfile.split("_")[0].replace("-","").split("/")[-1]

    for line in open(lfile,'r'):
        params = get_Giacomos_parameters(line,structure[benchmark])
        params["benchmark"] = benchmark
        if benchmark == "RHSFF":
            params["sin"] = 0.25
        if benchmark == "RHQDF":
            params["sin"] = 0.2
        if benchmark == "LHQDF1":
            params["sin"] = 0.1
        if benchmark == "LHQDF2":
            params["sin"] = 0.2
        if not (params['sin'],params["benchmark"]) in ref_plots.keys():
            ref_plots[(params["sin"],params["benchmark"])] = SinglePlot(params["sin"],params["benchmark"], "$m_{\phi} [GeV]$","$D_{\lambda,11}$","Cross-section [fb]")
            div_plots[(params["sin"],params["benchmark"])] = SinglePlot(params["sin"],params["benchmark"], "$m_{\phi} [GeV]$","$D_{\lambda,11}$","$\sigma/\sigma_{ref}$")

        if params["xsection"] < 0.0:
            print("Problem in file %s. Cannot find cross-section. Omitting this result."%line)
            continue

        ref_plots[(params["sin"],params["benchmark"])].append([params["mMed"] , params["dll"] , params["xsection"] * 1000.0] )
        ratio = plots[(params["sin"],params["benchmark"])].getValue(params["mMed"] , params["dll"]) / (params["xsection"] * 1000.0)
        if plots[(params["sin"],params["benchmark"])].getValue(params["mMed"] , params["dll"]) > 0:
            div_plots[(params["sin"],params["benchmark"])].append([params["mMed"] , params["dll"] , ratio])

for iD,plot in ref_plots.items():
    print("Plotting reference cross-sections for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("ref_xsections_sintheta%s_%s.pdf"%( str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    plot.plot("ref_xsections_sintheta%s_%s_log.pdf"%( str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)

for iD,plot in div_plots.items():
    print("Plotting ratio of cross-sections for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("ratio_sintheta%s_%s.pdf"%( str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
