import os,json,pandas,glob
from collections import OrderedDict

from lib.SinglePlot import *

def create_jo(joname,dsid,control_name):
    os.system("mkdir -p jos/%d"%dsid)
    os.system("echo \"include('%s')\" >  jos/%d/%s "%(control_name,dsid,joname))
    #os.system("echo ../%s >  jos/%d/%s "%(control_name,dsid,joname))
    os.system("cp ../run/%s ./jos/%d/"%(control_name,dsid))



def get_dsid(jobname):
    inFolders=glob.glob("old_jos/*")
    fily=None
    for fold in inFolders:
        inFile=glob.glob(fold+"/mc.*.py")[0]
        if jobname in inFile:
            fily=inFile
            break
    if not fily:
        return -999
    return int(fily.split("/")[1])

initial_events=10000
xsec=[]

#with open("pp13_stopsbottom_NNLO+NNLL.json","r") as f:
#    xsec=json.load(f)

with open("tcmet_signals_info_py8.json","r") as f:
    xsec=json.load(f)

points={
    #"RHSFF"  : [(1200,2.0),(1400,2.0),(1600,2.0),(1000,1.75),(1200,1.75),(1400,1.75),(1000,1.5),(1200,1.5),(1400,1.5),(800,1.25),(1000,1.25),(1200,1.25)] + [(400+x*200,0.5+y*0.25) for x in range(4) for y in range(3)],
    #"RHQDF"  : [(1200,2.0),(1400,2.0),(1600,2.0),(1000,1.75),(1200,1.75),(1400,1.75),(1000,1.5),(1200,1.5),(1400,1.5),(800,1.25),(1000,1.25),(1200,1.25)] + [(400+x*200,0.5+y*0.25) for x in range(4) for y in range(3)],
    #"LHQDF1" : [(1200,2.0),(1400,2.0),(1600,2.0),(1000,1.75),(1200,1.75),(1400,1.75),(1000,1.5),(1200,1.5),(1400,1.5),(800,1.25),(1000,1.25),(1200,1.25)] + [(400+x*200,0.5+y*0.25) for x in range(4) for y in range(3)],
    #"LHQDF2" : [(600+x*200,1.25+y*0.25) for x in range(5) for y in range(4)]
    #"RHSFF"  : [(400+x*200,0.0+y*0.25) for x in range(6) for y in range(4)] + [(300,0.25+y*0.25) for y in range(4)],
    #"RHQDF"  : [(400+x*200,0.0+y*0.25) for x in range(6) for y in range(4)] + [(300,0.25+y*0.25) for y in range(4)],
    #"LHQDF1" : [(400+x*200,0.0+y*0.25) for x in range(6) for y in range(4)] + [(300,0.25+y*0.25) for y in range(4)],
    #"LHQDF2" : [(600+x*200,0.75+y*0.25) for x in range(5) for y in range(1)] + [(500,1.), (600,1.)],
    "LHQDF1" : [(400,0.01),(600,0.01)]
    #"RHSFF"  : [(400+x*200,0.25+y*0.25) for x in range(6) for y in range(3)] + [(300,0.25+y*0.25) for y in range(4)] + [(400+x*200,0.1) for x in range(6)],
    #"RHQDF"  : [(400+x*200,0.25+y*0.25) for x in range(6) for y in range(3)] + [(300,0.25+y*0.25) for y in range(4)] + [(400+x*200,0.1) for x in range(6)],
    #"LHQDF1" : [(400+x*200,0.25+y*0.25) for x in range(6) for y in range(3)] + [(300,0.25+y*0.25) for y in range(4)] + [(400+x*200,0.1) for x in range(6)],
    #"LHQDF2" : [(600+x*200,0.75+y*0.25) for x in range(5) for y in range(1)] + [(500,1.), (600,1.)],
    #"RHSFF"  : [(400+x*200,0.75+y*0.25) for x in range(6) for y in range(6)] + [(300,1.25), (300,1.5), (300,1.75), (300,2.0)] + [(600,0.5),(800,0.5),(1600,2.0),(1800,2.0),(1600,1.75)],
    #"RHQDF"  : [(400+x*200,0.75+y*0.25) for x in range(6) for y in range(6)] + [(300,1.25), (300,1.5), (300,1.75), (300,2.0)] + [(600,0.5),(800,0.5),(1600,2.0),(1800,2.0),(1600,1.75)],
    #"LHQDF1" : [(400+x*200,0.75+y*0.25) for x in range(6) for y in range(6)] + [(300,1.25), (300,1.5), (300,1.75), (300,2.0)] + [(600,0.5),(800,0.5),(1600,2.0),(1800,2.0),(1600,1.75)],
    #"LHQDF2" : [(600+x*200,1.25+y*0.25) for x in range(5) for y in range(4)] + [(500,1.25), (500,1.5), (500,1.75), (500,2.0)] + [(800,1.0),(1000,1.0),(1200,1.0),(1600,2.0),(1800,2.0),(1600,1.75),(1800,1.75)],
}

#points={
points_already_there={
    #"RHSFF"  : [(400+x*200,0.75+y*0.25) for x in range(6) for y in range(6)] + [(300,1.25), (300,1.5), (300,1.75), (300,2.0)] + [(600,0.5),(800,0.5),(1600,2.0),(1800,2.0),(1600,1.75)],                   
    #"RHQDF"  : [(400+x*200,0.75+y*0.25) for x in range(6) for y in range(6)] + [(300,1.25), (300,1.5), (300,1.75), (300,2.0)] + [(600,0.5),(800,0.5),(1600,2.0),(1800,2.0),(1600,1.75)],                   
    #"LHQDF1" : [(400+x*200,0.75+y*0.25) for x in range(6) for y in range(6)] + [(300,1.25), (300,1.5), (300,1.75), (300,2.0)] + [(600,0.5),(800,0.5),(1600,2.0),(1800,2.0),(1600,1.75)],                   
    #"LHQDF2" : [(600+x*200,1.25+y*0.25) for x in range(5) for y in range(4)] + [(500,1.25), (500,1.5), (500,1.75), (500,2.0)] + [(800,1.0),(1000,1.0),(1200,1.0),(1600,2.0),(1800,2.0),(1600,1.75),(1800,1.75)],         
}
points_to_remove={
    #"RHSFF"  : [(1400,1.0),(1200,0.75),(1400,0.75)],
    #"RHQDF"  : [(1400,1.0),(1200,0.75),(1400,0.75)],
    #"LHQDF1" : [(1400,1.0),(1200,0.75),(1400,0.75)],
}



for bench,rpoints in points_to_remove.items():
    for rpoint in rpoints:
        if rpoint in points_already_there[bench]:
            points_already_there[bench].remove(rpoint)

for bench,rpoints in points_already_there.items():
    for rpoint in rpoints:
        if rpoint in points[bench]:
            points[bench].remove(rpoint)
#'''

commented='''
for bench,rpoints in points_to_remove.items():
    for rpoint in rpoints:
        if rpoint in points[bench]:
            points[bench].remove(rpoint)
'''

for bench,rpoints in points.items():
    print("\033[95m Requesting %d points for benchmark %s\033[0m"%(len(rpoints),bench))

#points=[(550,200),(600,200),(650,100),(650,200),(700,100),(750,100),(750,200),(700,350),(500,250),(400,200),(500,200),(600,100),(650,250)]

ecom=13000
typeSim="FullSim"
priority=1 ## 1 for high priority, 2 for normal
factor=4.
evgenRelease="21.6.97"
comments="" #"Signal grid request"

structure=["DSID","Event input for evgen (optional)","E_CoM [GeV]","Output events","Type (Evgen, FullSim, AF2, FCSv2, FastChain, ...)","Priority","Output formats","Evgen Release","Comments","Evgen tag","Evgen merge tag","Simul tag","Merge tag","Digi tag","Reco tag","Rec Merge tag","Deriv tag","Deriv merge tag"]


mcCamp=OrderedDict([('mc16a',36.2),('mc16d',44.3),('mc16e',58.5)])

dictionary=OrderedDict()
summary={}

for mc,lumi_target in mcCamp.items():
    dictionary[mc]=OrderedDict()
    summary[mc]=0.0
    DSID=100000
    for st in structure:
        dictionary[mc][st]=[]

    print("\n \033[92m========== Doing %s ==========\033[0m"%mc)

    for bench,list_points in points.items():
        cross=xsec[bench]
        for idsid in range(len(list_points)):
            sintheta = "s0p2"
            if bench == "RHSFF":
                sintheta = "s0p25"
            if bench == "LHQDF1":
                sintheta = "s0p1"

            sigName="m%d_d%s_%s_%s"%(list_points[idsid][0],str(list_points[idsid][1]).replace(".","p"),sintheta,bench)

            branching_ratio = 1.0
            if "d0p1_" in sigName or ("d0p0_" in sigName and not sigName in cross.keys()) or "d0p01_" in sigName:
                print("\033[91mMissing cross-section number of %s\033[0m"%sigName)
                sigName="m%d_d%s_%s_%s"%(list_points[idsid][0],"0p25",sintheta,bench)
                branching_ratio = 0.5
            real_xsection=cross[sigName][2]
            #branching_ratio = cross[sigName][6]
            filtereff = cross[sigName][3]
            
            request_events=10000
            while(request_events*branching_ratio < factor*real_xsection*filtereff*lumi_target ) and request_events<150000.0:
                request_events+=10000

            if request_events == 10000:
                request_events=20000

            comments="mc.MGPy8EG_A14N30NLO_m%d_d%s_%s_%s_1LorMET.py"%(list_points[idsid][0],str(list_points[idsid][1]).replace(".","p"),sintheta,bench)

            #DSID=get_dsid(comments)
            if DSID < 0.0:
                print("\033[91mCannot find DSID in old job option for joboption %s\033[0m"%comments)
                continue
            print("DSID %d for point (%d,%.2f) must have %d events"%(DSID,list_points[idsid][0],list_points[idsid][1],request_events))

            create_jo("mc.MGPy8EG_A14N30NLO_m%d_d%s_%s_%s_1LorMET.py"%(list_points[idsid][0],str(list_points[idsid][1]).replace(".","p"),sintheta,bench),DSID,"MadGraphControl_DMFV.py") 

            dictionary[mc]["DSID"].append(DSID)
            dictionary[mc]["Event input for evgen (optional)"].append("")
            dictionary[mc]["E_CoM [GeV]"].append(ecom)
            dictionary[mc]["Output events"].append(request_events)
            dictionary[mc]["Type (Evgen, FullSim, AF2, FCSv2, FastChain, ...)"].append(typeSim)
            dictionary[mc]["Priority"].append(priority)
            dictionary[mc]["Output formats"].append("")
            dictionary[mc]["Evgen Release"].append(evgenRelease)
            dictionary[mc]["Comments"].append(comments)
            dictionary[mc]["Evgen tag"].append("e8470")
            dictionary[mc]["Evgen merge tag"].append("")
            dictionary[mc]["Simul tag"].append("")
            dictionary[mc]["Merge tag"].append("")
            dictionary[mc]["Digi tag"].append("")
            dictionary[mc]["Reco tag"].append("")
            dictionary[mc]["Rec Merge tag"].append("")
            dictionary[mc]["Deriv tag"].append("")
            dictionary[mc]["Deriv merge tag"].append("")
            summary[mc]+=request_events
            DSID+=1
    pd=pandas.DataFrame(data=dictionary[mc])
    print(pd)
    pd.to_csv("request_%s.csv"%mc)


plots={}

for bench, samples in xsec.items():
    sintheta = 0.2
    if bench == "RHSFF":
        sintheta = 0.25
    if bench == "LHQDF1":
        sintheta = 0.1

    if not (sintheta,bench) in plots.keys():
        plots[(sintheta,bench)]=SinglePlot(sintheta,bench, "$m_{\phi} [GeV]$","$D_{\lambda,11}$","Cross-section [fb]")

    for sample_name,chars in samples.items():
        plots[(sintheta,bench)].append([ chars[0], chars[1] , chars[2]] )

    
for iD,plot in plots.items():
    print("Plotting request against cross-sections values for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    bench=iD[1]
    #plot.plot("request_plot_%s.pdf"%(bench) , plotNumbers = False, points_to_plot = points[bench])
    #plot.plot("request_plot_%s.pdf"%(bench) , plotNumbers = False, points_to_plot = points[bench],already_there=points_already_there[bench])


print("\n \033[92m========== Summary ==========\033[0m")

number_of_points = 0
for bench,lpoints in points.items():
    number_of_points+=len(lpoints)
print("Requesting %d points"%number_of_points)
    
for mc,tot_events in summary.items():
    print("Number of events in %s : %d events"%(mc,tot_events))
print("Total number of events :  %d"%sum(summary.values()))
