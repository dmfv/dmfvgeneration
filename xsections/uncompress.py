import os,glob
import json
from lib.SinglePlot import *

currentDir = os.getcwd()


plots={}
filteff={}

benchmark={} ## For SimpleAnalysis
#for folder in glob.glob("PersonalGeneration/*log"):
for folder in glob.glob("production/*log"):
    print("Processing %s"%folder)
    
    dsid=folder.split("/")[-1].split(".")[6]
    
    infiles=glob.glob(folder+"/*tgz")
    if len(infiles) == 0:
        continue
    infile=infiles[0]
    
    outlines=os.popen("tar xvfz %s"%infile).readlines()
    tarball = ""
    for liny in outlines:
        liny=liny.split("\n")[0]
        if "tarball" in liny:            
            tarball = liny.split("/")[0]
            break
    print("TarBall name : %s"%tarball)
    if tarball == "" :
        continue
    physicsProcess=None
    xsection=None
    filtereff=None
    for inline in open(tarball+"/log.generate","r"):
        inline=inline.split("\n")[0]
        if "physicsShort less than 50 characters" in inline:
            physicsProcess=inline.split("physicsShort")[0].split("OK: ")[-1].rstrip()
        if "Cross-section" in inline:
            xsection=float(inline.split("+-")[0].split("Cross-section :   ")[-1].rstrip())*1000 ## transform it into pb
        if "MetaData: GenFiltEff =" in inline:
            filtereff=float(inline.split("+-")[0].split("MetaData: GenFiltEff =")[-1].rstrip()) ## transform it into pb
        if xsection and physicsProcess and filtereff:
            break

    if not xsection or not physicsProcess or not filtereff:
        os.system("rm -rf %s"%tarball)
        continue

    if xsection > 10000000 : ## buggy points
        os.system("rm -rf %s"%tarball)
        continue
    
    mmed = float(physicsProcess.split("_")[2].replace("m",""))
    delta = float(physicsProcess.split("_")[3].replace("d","").replace("p","."))
    sintheta = float(physicsProcess.split("_")[4].replace("s","").replace("p","."))
    bench = physicsProcess.split("_")[5]
    
    ## Filling DSID,bnechmark --> mass,delta information
    if not bench in benchmark.keys():
        benchmark[bench] = {}

    if not dsid in benchmark[bench].keys():
        benchmark[bench][dsid]=[mmed,delta,xsection,filtereff]

    if not (sintheta,bench) in plots.keys():
        plots[(sintheta,bench)] = SinglePlot(sintheta,bench, "$m_{\phi} [GeV]$","$D_{\lambda,11}$","Cross-section [fb]")
    if not (sintheta,bench) in filteff.keys():
        filteff[(sintheta,bench)] = SinglePlot(sintheta,bench, "$m_{\phi} [GeV]$","$D_{\lambda,11}$","Filter efficiency")

    if xsection < 0.0:
        print("Problem in logfile %s. Cannot find cross-section. Omitting this result."%lfile)
        continue

    plots[(sintheta,bench)].append([mmed , delta , xsection] )
    filteff[(sintheta,bench)].append([mmed , delta , filtereff] )

    os.system("rm -rf %s"%tarball)


for benchy,dsids in benchmark.items():
    for dsid,mass in dsids.items():
        print("benchmark[\"%s\"][\"%s\"] = [%.1f,%.2f]"%(benchy,dsid,mass[0],mass[1]))

with open("tcmet_signals_info.json","w+") as f:
    json.dump(benchmark,f,indent=4)

for iD,plot in plots.items():
    print("Plotting cross-sections for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("xsections_tcmet_sintheta%s_%s.pdf"%( str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    plot.plot("xsections_tcmet_sintheta%s_%s_log.pdf"%( str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)

for iD,plot in filteff.items():
    print("Plotting cross-sections for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("filteff_tcmet_sintheta%s_%s.pdf"%( str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    plot.plot("filteff_tcmet_sintheta%s_%s_log.pdf"%( str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)


