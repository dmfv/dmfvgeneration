#!/bin/bash

#folders=( /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/original_production/ /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/goodmass_production/ /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production/fgen/  /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production/fgenall/  /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production/fgenlight/  /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production/pp/  /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production/ppall/  /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production/pplight/  /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production/schanall/  /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production/schanlight/ )
folders=( /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production_chiul/fgen/  /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production_chiul/fgenall/  /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production_chiul/fgenlight/ /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production_chiul/ppall/  /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production_chiul/pplight/  /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production_chiul/schanall/  /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production_chiul/schanlight/ )


for folder in ${folders[*]}
do
    python get_values.py -f $folder
done
