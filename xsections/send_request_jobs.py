import os,glob

jos=glob.glob("jos/*")

os.system("mkdir -p condor/")

nevents=10000
gensubmitter=open("submit_condor.sh","w")

for jo in jos:
    
    joboption = jo.split("/")[-1]
    if not joboption.startswith("100"):
        continue
    print("Processing JobOption %s"%jo)

    submitter=open("condor/submit_%s.submit"%joboption,"w")
    fw=open("condor/generate_%s.sh"%joboption,"w")
    fw.write("#!/bin/bash\n")
    fw.write("mkdir -p /tmp/alopezso/submission/%s/\n"%joboption)
    fw.write("cd /tmp/alopezso/submission/%s/\n"%joboption)
    fw.write("export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase\n")
    fw.write("source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh \n")
    fw.write("asetup AthGeneration,21.6.97,here\n")
    fw.write("cp -r %s ./\n"%(os.getcwd()+"/"+jo))
    #fw.write("export PYTHONPATH=%s/../models/:%s\n"%(os.getcwd(),os.getenv("PYTHONPATH")))                                                                                     
    fw.write("export PYTHONPATH=%s/../models/:$PYTHONPATH\n")
    fw.write("Gen_tf.py --ecmEnergy=13000 --firstEvent=1  --maxEvents=%d --randomSeed=1234 --jo=%s --outputEVNTFile=tmp.root --localPath=/tmp/alopezso/submission/%s/\n"%(nevents,joboption,joboption))
    fw.write("cp log.generate %s\n"%(os.getcwd()+"/"+jo))
    fw.write("rm -rf /tmp/alopezso/submission/%s/\n"%joboption)
    
    # Setting up how                                                                                                                                                                                 
    submitter.write("# job %s\n"%joboption)
    submitter.write("universe = vanilla\n")
    submitter.write("executable = %s\n"%(os.getcwd()+"/condor/generate_%s.sh"%joboption))
    submitter.write("+JobFlavour = \"tomorrow\"\n")
    #submitter.write("getenv=True\n")
    submitter.write("request_memory=1024*10\n")
    submitter.write("output=condor/%s.EVNT.out\n"%joboption)
    submitter.write("log=condor/%s.EVNT.log\n"%joboption)
    submitter.write("error=condor/%s.EVNT.err\n"%joboption)
    submitter.write("RequestCpus = 1\n")
    submitter.write("queue 1\n")

    gensubmitter.write("condor_submit "+os.getcwd()+"/condor/submit_%s.submit\n"%joboption)
    
    submitter.close()
    fw.close()
    os.system("chmod 777 "+os.getcwd()+"/condor/generate_%s.sh"%joboption)
    os.system("chmod 777 "+os.getcwd()+"/condor/submit_%s.submit"%joboption)

gensubmitter.close()
#os.system("source submit_condor.sh")
