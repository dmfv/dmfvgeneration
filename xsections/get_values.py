import os,glob,argparse,math
import json
from lib.SinglePlot import *


parser=argparse.ArgumentParser("Get the cross-section plots for the DMFV samples")
parser.add_argument("-f","--basefolder",default="/lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/original_production/",help="Folder")
parser.add_argument("-o","--outputfolder",default=None,help="Output folder. If none is given, the basefolder is used.")
args=parser.parse_args()

currentDir = os.getcwd()

basefolder=args.basefolder
outputFolder=args.basefolder

if args.outputfolder:
    outputFolder=args.outputfolder

plots={}
filteff={}
bru_plots={}
brc_plots={}
brt_plots={}
brd_plots={}
brs_plots={}
brb_plots={}
width_plots={}
widthup_plots={}
widthdw_plots={}
widthratio_plots={}
widthupratio_plots={}
widthdwratio_plots={}
time_plots={}
timedw_plots={}

benchmark={} ## For SimpleAnalysis

for infile in glob.glob("%s/*inclus.EVNT.log"%basefolder):
    print("Processing %s"%infile)

    physicsProcess=None
    xsection=None
    filtereff=None
    width,time=None,None
    width_dw,time_dw=None,None
    brU,brD,brC,brS,brB,brT,brTadd,brUadd = None,None,None,None,None,None,None,None

    for inline in open(infile,"r"):
        inline=inline.split("\n")[0]
        if "physicsShort less than 50 characters" in inline:
            physicsProcess=inline.split("physicsShort")[0].split("OK: ")[-1].rstrip()
        if "Cross-section" in inline:
            xsection=float(inline.split("+-")[0].split("Cross-section :   ")[-1].rstrip())*1000 ## transform it into fb
        if "MetaData: GenFiltEff =" in inline:
            filtereff=float(inline.split("+-")[0].split("MetaData: GenFiltEff =")[-1].rstrip()) ## transform it into pb

        if "_RH" in infile:
            if "phi              phi~               2   2   1" in inline:
                allitems = [x.rstrip() for x in inline.split("phi              phi~               2   2   1 ")[-1].split(" ") if x.rstrip() !='']
                width=float(allitems[1])
                time=float(allitems[4])
        elif "_LH" in infile:
            if "phiup            phiup~             1   2   1" in inline:
                allitems = [x.rstrip() for x in inline.split("phiup            phiup~             1   2   1 ")[-1].split(" ") if x.rstrip() !='']
                width=float(allitems[1])
                time=float(allitems[4])
            if "phidown          phidown~           1  -1   1" in inline:
                allitems = [x.rstrip() for x in inline.split("phidown          phidown~           1  -1   1 ")[-1].split(" ") if x.rstrip() !='']
                width_dw=float(allitems[1])
                time_dw=float(allitems[4])
        if "100        6 -9900025" in inline:
            brT=float(inline.split("  100        6 -9900025")[0].split(" 1 ")[-1].rstrip()) 
        if "100        4 -9900023" in inline:
            brC=float(inline.split("  100        4 -9900023")[0].split(" 1 ")[-1].rstrip()) 
        if "100        2 -9900022" in inline:
            brU=float(inline.split("  100        2 -9900022")[0].split(" 1 ")[-1].rstrip()) 
        if "100        5 -9900025" in inline:
            brB=float(inline.split("  100        5 -9900025")[0].split(" 1 ")[-1].rstrip()) 
        if "100        3 -9900023" in inline:
            brS=float(inline.split("  100        3 -9900023")[0].split(" 1 ")[-1].rstrip()) 
        if "100        1 -9900022" in inline:
            brD=float(inline.split("  100        1 -9900022")[0].split(" 1 ")[-1].rstrip()) 

        
        if "100        2 -9900025" in inline:
            brUadd=float(inline.split("100        2 -9900025")[0].split(" 1 ")[-1].rstrip().lstrip())
        if "100        6 -9900022" in inline:
            brTadd=float(inline.split("100        6 -9900022")[0].split(" 1 ")[-1].rstrip().lstrip())

        if xsection and width and physicsProcess and filtereff and brC and brT and brU and brUadd and brTadd and (( "LHQSF" in infile and brD and brS and brB) or ("_RH" in infile)):
            break
        #if xsection and width and physicsProcess and filtereff and brC and brT and brU and (( "LHQSF" in infile and brD and brS and brB) or ("_RH" in infile)):
        #    break


    mmed,delta,sintheta,bench=None,None,None,None
    if not xsection or not physicsProcess or not filtereff:
        xsection=-999.0
        filtereff=-999.0
        mmed = float(infile.split("/")[-1].split("_")[0].replace("m",""))
        delta = float(infile.split("/")[-1].split("_")[1].replace("d","").replace("p","."))
        sintheta = float(infile.split("/")[-1].split("_")[2].replace("s","").replace("p","."))
        bench = infile.split("/")[-1].split("_")[3].split(".")[0]
        #continue
    else:
        mmed = float(physicsProcess.split("_")[2].replace("m",""))
        delta = float(physicsProcess.split("_")[3].replace("d","").replace("p","."))
        sintheta = float(physicsProcess.split("_")[4].replace("s","").replace("p","."))
        bench = physicsProcess.split("_")[5].split(".")[0]


    if not brU and not brC and not brT:
        continue # buggy points
    if not brU:
        brU=-999.0
    if not brD:
        brD=-999.0
    if not brS:
        brS=-999.0
    if not brC:
        brC=-999.0
    if not brB:
        brB=-999.0
    if not brT:
        brT=-999.0
    if not brUadd:
        brUadd=0.0
    if not brTadd:
        brTadd=0.0

    brU = brU+brUadd
    brT = brT+brTadd

    if not width:
        width=-999.0
    if not width_dw:
        width_dw=-999.0
    if not time:
        time=-999.0
    else:
        time=time*math.pow(10,20)/(3*math.pow(10,11)) # Expressed initially as mm/c. Numbers too small to express in seconds. Multiply lifetime by 10^{20} and transform mm/c into seconds.

    if not time_dw:
        time_dw=-999.0
    else:
        time_dw=time_dw*math.pow(10,20)/(3*math.pow(10,11))  # Expressed initially as mm/c. Numbers too small to express in seconds. Multiply lifetime by 10^{20} and transform mm/c into seconds.

    if xsection > 10000000 or width < 0.0 or (width_dw < 0.0 and "_LH" in infile) or time < 0 or (time_dw < 0.0 and "_LH" in infile): ## buggy points
        continue

    
    ## Filling DSID,bnechmark --> mass,delta information
    if not bench in benchmark.keys():
        benchmark[bench] = {}
    if not "m%d_d%s_s%s_%s"%(mmed,str(delta).replace(".","p"),str(sintheta).replace(".","p"),bench) in benchmark[bench].keys():
        benchmark[bench]["m%d_d%s_s%s_%s"%(mmed,str(delta).replace(".","p"),str(sintheta).replace(".","p"),bench)]=[]

    benchmark[bench]["m%d_d%s_s%s_%s"%(mmed,str(delta).replace(".","p"),str(sintheta).replace(".","p"),bench)].extend((mmed,delta,xsection,filtereff,brU,brD,brC,brS,brT,brB))

    if not (sintheta,bench) in plots.keys():
        plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$","Cross-section [fb]")
    if not (sintheta,bench) in filteff.keys():
        filteff[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$","Filter efficiency")
    if not (sintheta,bench) in brc_plots.keys():
        if "_RH" in infile:
            bru_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$","BR ($\phi \\rightarrow u\chi$)")
            brc_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$","BR ($\phi \\rightarrow c\chi$)")
            brt_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$","BR ($\phi \\rightarrow t\chi$)")
            if not (sintheta,bench) in width_plots.keys():
                width_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$",r"Mediator width ($\Gamma_{\phi}$) [GeV]")
                time_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$",r"Mediator lifetime $(\tau_{\phi})*10^{20}$ [s]")
                widthratio_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$",r"$\Gamma_{\phi}$/$m_{\phi}$ [%]")
        if "_LHQDF" in infile:
            bru_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$","BR ($\phi_{u} \\rightarrow u\chi$)")
            brc_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$","BR ($\phi_{u} \\rightarrow c\chi$)")
            brt_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$","BR ($\phi_{u} \\rightarrow t\chi$)")
            brd_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$","BR ($\phi_{d} \\rightarrow d\chi$)")
            brs_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$","BR ($\phi_{d} \\rightarrow s\chi$)")
            brb_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$","BR ($\phi_{d} \\rightarrow b\chi$)")
            if not (sintheta,bench) in width_plots.keys():
                width_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"m_{\phi_{u}} [GeV]",r"$D_{\lambda,11}$",r"Mediator width ($\Gamma_{\phi_{u}}$) [GeV]")
                time_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"m_{\phi_{u}} [GeV]",r"$D_{\lambda,11}$",r"Mediator lifetime $(\tau_{\phi_{u}})*10^{20}$ [s]")
                widthdw_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"m_{\phi_{d}} [GeV]",r"$D_{\lambda,11}$",r"Mediator width ($\Gamma_{\phi_{d}}$) [GeV]")
                timedw_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"m_{\phi_{d}} [GeV]",r"$D_{\lambda,11}$",r"Mediator lifetime $(\tau_{\phi_{d}})*10^{20}$ [s]")
                widthratio_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$",r"$\Gamma_{\phi_{u}}/m_{\phi_{u}}$ [%]")
                widthdwratio_plots[(sintheta,bench)]=SinglePlot(sintheta,bench, r"$m_{\phi}$ [GeV]",r"$D_{\lambda,11}$",r"$\Gamma_{\phi_{d}}/m_{\phi_{d}}$ [%]")
    if xsection < 0.0:
        print("\033[91mProblem in logfile %s. Cannot find cross-section. Omitting this result.\033[0m"%infile)
        continue

    plots[(sintheta,bench)].append([mmed , delta , xsection] )
    filteff[(sintheta,bench)].append([mmed , delta , filtereff] )


    if brU > -0.0001:
        bru_plots[(sintheta,bench)].append([mmed , delta , brU] )
    if brC > -0.0001:
        brc_plots[(sintheta,bench)].append([mmed , delta , brC] )
    if brT > -0.0001:
        brt_plots[(sintheta,bench)].append([mmed , delta , brT] )

    width_plots[(sintheta,bench)].append([mmed , delta , width] )
    time_plots[(sintheta,bench)].append([mmed , delta , time] )
    widthratio_plots[(sintheta,bench)].append([mmed , delta , width*100/mmed] )

    if "_LHQDF" in infile:
        if brD > -0.0001:
            brd_plots[(sintheta,bench)].append([mmed , delta , brD] )
        if brS > -0.0001:
            brs_plots[(sintheta,bench)].append([mmed , delta , brS] )
        if brB > -0.0001:
            brb_plots[(sintheta,bench)].append([mmed , delta , brB] )

        widthdw_plots[(sintheta,bench)].append([mmed , delta , width_dw] )
        timedw_plots[(sintheta,bench)].append([mmed , delta , time_dw] )
        widthdwratio_plots[(sintheta,bench)].append([mmed , delta , width_dw*100/mmed] )

os.system("mkdir -p %s/plots/"%outputFolder)
#with open("tcmet_signals_info_dmfv_vs_susy.json","w+") as f:


with open("%s/plots/tcmet_signals_info.json"%outputFolder,"w+") as f: ## Original name
    json.dump(benchmark,f,indent=4)

for iD,plot in plots.items():
    print("Plotting cross-sections for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("%s/plots/xsections_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    #plot.plot("%s/plots/xsections_tcmet_sintheta%s_%s_log.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)

for iD,plot in filteff.items():
    print("Plotting filter efficiencies for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("%s/plots/filteff_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    #plot.plot("%s/plots/filteff_tcmet_sintheta%s_%s_log.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)

for iD,plot in bru_plots.items():
    print("Plotting branching ratios of the mediator to u-jet for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("%s/plots/brU_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    #plot.plot("%s/plots/brU_tcmet_sintheta%s_%s_log.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)
for iD,plot in brc_plots.items():
    print("Plotting branching ratios of the mediator to c-jet for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("%s/plots/brC_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    #plot.plot("%s/plots/brC_tcmet_sintheta%s_%s_log.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)
for iD,plot in brt_plots.items():
    print("Plotting branching ratios of the mediator to top for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("%s/plots/brT_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    #plot.plot("%s/plots/brT_tcmet_sintheta%s_%s_log.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)

for iD,plot in brd_plots.items():
    print("Plotting branching ratios of the mediator to d-quark for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("%s/plots/brD_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    #plot.plot("%s/plots/brD_tcmet_sintheta%s_%s_log.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)
for iD,plot in brs_plots.items():
    print("Plotting branching ratios of the mediator to s-quark for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("%s/plots/brS_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    #plot.plot("%s/plots/brS_tcmet_sintheta%s_%s_log.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)
for iD,plot in brb_plots.items():
    print("Plotting branching ratios of the mediator to b-quark for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("%s/plots/brB_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    #plot.plot("%s/plots/brB_tcmet_sintheta%s_%s_log.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)

for iD,plot in time_plots.items():
    if "LH" in iD[1]:
        print("Plotting the total time of the mediator up for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
        plot.plot("%s/plots/timeup_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    else:
        print("Plotting the total time of the mediator for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
        plot.plot("%s/plots/time_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    #plot.plot("%s/plots/time_tcmet_sintheta%s_%s_log.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)


for iD,plot in timedw_plots.items():
    print("Plotting the total time of the mediator down for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("%s/plots/timedw_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)


for iD,plot in width_plots.items():
    if "LH" in iD[1]:
        print("Plotting the total width of the mediator up for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
        plot.plot("%s/plots/widthup_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    else:
        print("Plotting the total width of the mediator for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
        plot.plot("%s/plots/width_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    #plot.plot("%s/plots/width_tcmet_sintheta%s_%s_log.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)


for iD,plot in widthdw_plots.items():
    print("Plotting the total width of the mediator down for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("%s/plots/widthdw_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)



for iD,plot in widthratio_plots.items():
    if "LH" in iD[1]:
        print("Plotting the total width of the mediator up for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
        plot.plot("%s/plots/widthupratio_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    else:
        print("Plotting the total width of the mediator for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
        plot.plot("%s/plots/widthratio_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
    #plot.plot("%s/plots/width_tcmet_sintheta%s_%s_log.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True,log = True)


for iD,plot in widthdwratio_plots.items():
    print("Plotting the total width of the mediator down for benchmark %s and sin theta = %s"%(str(iD[1]),str(iD[0])))
    plot.plot("%s/plots/widthdwratio_tcmet_sintheta%s_%s.pdf"%( outputFolder,str(iD[0]).replace(".","p") , iD[1]) , plotNumbers = True)
