import os,glob,json


with open("tcmet_signals_info_py8.json","r") as f:
    xsec=json.load(f)



logfiles = glob.glob("jos/5*/log.generate")

for inlog in logfiles:
    f=open(inlog,"r")

    physicsProcess=None
    xsection=None
    filtereff=None
    for inline in f:
        inline=inline.split("\n")[0]
        if "physicsShort less than 50 characters" in inline:
            physicsProcess=inline.split("physicsShort")[0].split("OK: ")[-1].rstrip()
        if "Cross-section" in inline:
            xsection=float(inline.split("+-")[0].split("Cross-section :   ")[-1].rstrip())*1000 ## transform it into pb                                                                                    
        if "MetaData: GenFiltEff =" in inline:
            filtereff=float(inline.split("+-")[0].split("MetaData: GenFiltEff =")[-1].rstrip()) ## transform it into pb                                                                                    
        if xsection and physicsProcess and filtereff:
            break

    mmed,delta,sintheta,bench=None,None,None,None

    mmed = physicsProcess.split("_")[2].replace("m","")
    delta = physicsProcess.split("_")[3].replace("d","")
    sintheta = physicsProcess.split("_")[4].replace("s","")
    bench = physicsProcess.split("_")[5].split(".")[0]
    cross=xsec[bench]
    sigName="m%s_d%s_s%s_%s"%(mmed,delta,sintheta,bench)
    if not xsection==cross[sigName][2] or not filtereff == cross[sigName][3]:
        print("\033[91mProblem in jo %s for signal %s : xsec/filtereff in json %.3f,%.3f and xsec/filtereff in new file %.3f,%.3f\033[0m"%(inlog,sigName,cross[sigName][2],cross[sigName][3],xsection,filtereff))
    else:
        print("\033[95mOK for jo %s for signal %s \033[0m"%(inlog,sigName))
    f.close()
