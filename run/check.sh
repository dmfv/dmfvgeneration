#!/bin/bash

types=( s0p25_RHSFF s0p2_RHQDF s0p1_LHQDF1 s0p2_LHQDF2)
vars=( schan pp fgen )
datas=( 0p25 1p0 2p0 )

mass=800

for intype in ${types[*]};
do

    echo "===================="
    echo "$intype"
    echo "===================="
    echo ""

    for data in ${datas[*]};
    do
	echo $data
	echo "*************"
	for var in ${vars[*]};
	do
	    echo "$var"
	    cat /lustre/fs24/group/atlas/alopezso/DMFV/xsecProportion/m${mass}_d${data}_${intype}_${var}.EVNT.log | grep "Cross-section"
	done
	echo "*************"
	echo 
    done
done
