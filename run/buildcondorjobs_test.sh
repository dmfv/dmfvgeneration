#!/bin/bash                                                                                                                                                                                               
gensubmitter=submit.sh
tmpFolder=/tmp/alopezso/

mMeds=( 1000 )
dcoups=( 0p0 0p2 0p6 1p0 1p4 2p0 )
dcoups=( 0p6 1p0 1p4 2p0 )
procs=( schanlight pplight fgenlight )
#procs=( ppall )
procs=( schan schanlight schanall pp pplight ppall fgen fgenall fgenlight )
procs=( schanlight schanall pplight ppall fgen fgenall fgenlight )
procs=( gia1 gia2 gia3 gia4 gia5 gia6 gia7 gia8 gia9 gia10 gia11 gia12 gia13 gia14)


#dcoups=( 0p0 0p01 0p05 0p1 0p15 0p2 0p3 0p4 )
#procs=( gia10 gia11 )
#procs=( gia1 gia3 gia5 gia6 gia13 )
#procs=( gia1 gia3 )
procs=(gia15)
sins=( 0p1 0p2 0p25 )
benchmarks=( LHQDF2 LHQDF1 RHSFF RHQDF )
#benchmarks=( LHQDF2 LHQDF1 )
benchmarks=( RHSFF RHQDF )
nevents=10000


MadGraphConfig=MadGraphControl_DMFV_test.py
stem=mc.MGPy8EG_A14N30NLO_m1500_d1p0_s0p2_LHQDF2_1LorMET.py

outSpace=/lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/test_production_chiul_bwcutoffout/


mkdir -p $outSpace/

DSID=700000

date=22112023

mkdir -p logs/
rm $gensubmitter
chmod 777 $gensubmitter
echo "#!/bin/bash" > $gensubmitter

for proc in ${procs[*]}
do
    mkdir -p $outSpace/$proc/
    for mMed in ${mMeds[*]}
    do
	for dcoup in ${dcoups[*]}
	do
	    for sin in ${sins[*]}
	do
		for benchmark in ${benchmarks[*]}
		do
		    if [[ $benchmark == LHQDF1 ]];
		    then
		    if [[ $sin != 0p1 ]];
		    then
			continue
		    fi
		    elif [[ $benchmark == LHQDF2 ]];
		    then
			if [[ $sin != 0p2 ]];
			then
			continue
			fi
		    elif [[ $benchmark == RHSFF ]];
		    then
			if [[ $sin != 0p25 ]];
			then
			    continue
			fi
		    elif [[ $benchmark == RHQDF ]];
		    then
			if [[ $sin != 0p2 ]];
			then
			    continue
			fi
		    fi
		    echo "Processing MMED : ${mMed} dLL : ${dcoup}, st : $sin and benchmark : $benchmark"
		    file=${stem%A14N30NLO_*}_m${mMed}_d${dcoup}_s${sin}_${benchmark}_${proc}_1LorMET.py
		    
		    ## Building the specific jobOption                                                                                                                                                                
		    rm -rf $PWD/$DSID
		    mkdir -p $PWD/$DSID
		    echo "include(\"$MadGraphConfig\")" > $PWD/$DSID/$file 
		    
		    submitter=logs/submit_m${mMed}_d${dcoup}_s${sin}_${benchmark}_${proc}.submit
		    jobOption=logs/generate_m${mMed}_d${dcoup}_s${sin}_${benchmark}_${proc}.sh
		    rm $submitter $jobOption
		    echo "" > $submitter
		    echo "#!/bin/bash" > $jobOption
		    echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase" >> $jobOption
		    echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh " >> $jobOption
		    echo "asetup AthGeneration,21.6.97,here" >> $jobOption
		    echo "mkdir -p ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${proc}/" >> $jobOption
		    echo "ls" >> $jobOption
		    echo "pwd" >> $jobOption
		    echo "cd ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${proc}/" >> $jobOption
		    echo "cp -r $PWD/$DSID/ ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${proc}/" >> $jobOption
		    echo "cp $PWD/$MadGraphConfig ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${proc}/" >> $jobOption
		    echo "export PYTHONPATH=$PWD/../models/:\$PYTHONPATH" >> $jobOption                                                                                     
		    echo "Gen_tf.py --ecmEnergy=14000 --firstEvent=1  --maxEvents=$nevents --randomSeed=1234 --jo=$DSID --outputEVNTFile=${file%.py*}.root --localPath=${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${proc}/$DSID" >>$jobOption
		    echo "cp log.generate $outSpace/${proc}/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${proc}.EVNT.log" >> $jobOption
                    echo "rm -rf ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${proc}/" >> $jobOption
		    
		    # Setting up how                                                                                                                                                                                 
		    echo "# job ${stem}_m${mMed}_d${dcoup}_s${sin}_${benchmark}_${proc}" >> $submitter
		    echo "universe = vanilla" >> $submitter
		    echo "executable = $jobOption" >> $submitter
		    echo "+JobFlavour = \"tomorrow\"" >> $submitter
		    echo "output= logs/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${proc}.EVNT.out" >> $submitter
		    echo "log= logs/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${proc}.EVNT.log" >> $submitter
		    echo "error= logs/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${proc}.EVNT.err" >> $submitter
		    echo "getenv=True" >> $submitter
		    echo "RequestCpus = 1" >> $submitter
		    echo "queue 1" >> $submitter
		    
		    echo "condor_submit $submitter" >> $gensubmitter
		    
		    chmod 777 $submitter 
		    chmod 777 $jobOption
		    DSID=$((DSID+1))
		done
	    done
	done
    done
done
