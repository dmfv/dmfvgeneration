#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
folder=1000001
asetup AthGeneration,21.6.97,here
mkdir -p /tmp/alopezso/myint/
ls
pwd
cd /tmp/alopezso/myint/
cp -r /afs/ifh.de/group/atlas/users/alopezso/DMFV/simulateDMFV/run/$folder/ /tmp/alopezso/myint/
cp /afs/ifh.de/group/atlas/users/alopezso/DMFV/simulateDMFV/run/MadGraphControl_DMFV_test.py /tmp/alopezso/myint/
nohup Gen_tf.py --ecmEnergy=13000 --firstEvent=1  --maxEvents=10000 --randomSeed=1234 --jo=$folder --outputEVNTFile=test.root --localPath=/tmp/alopezso/myint/$folder &
#rm -rf /tmp/alopezso/myint/
