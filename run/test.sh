#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh 
asetup AthGeneration,21.6.97,here
mkdir -p /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/uncertainties_production//m900_d1p5_s0p2_RHQDF_inclus/
ls
pwd
cd /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/uncertainties_production//m900_d1p5_s0p2_RHQDF_inclus/
cp -r /afs/ifh.de/group/atlas/users/alopezso/DMFV/simulateDMFV/run/500242/ /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/uncertainties_production//m900_d1p5_s0p2_RHQDF_inclus/
cp /afs/ifh.de/group/atlas/users/alopezso/DMFV/simulateDMFV/run/MadGraphControl_DMFV_final.py /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/uncertainties_production//m900_d1p5_s0p2_RHQDF_inclus/
export PYTHONPATH=/afs/ifh.de/group/atlas/users/alopezso/DMFV/simulateDMFV/run/../models/:$PYTHONPATH
Gen_tf.py --ecmEnergy=13000 --firstEvent=1  --maxEvents=30000 --randomSeed=1234 --jo=500242 --outputEVNTFile=mc.MGPy8EG_A14_m900_d1p5_s0p2_RHQDF_1LorMET_inclus.root --localPath=/lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/uncertainties_production//m900_d1p5_s0p2_RHQDF_inclus/500242
#cp log.generate /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/uncertainties_production//m900_d1p5_s0p2_RHQDF_inclus.EVNT.log
#cp mc.MGPy8EG_A14_m900_d1p5_s0p2_RHQDF_1LorMET_inclus.root /lustre/fs22/group/atlas/alopezso/tcMeT/dmfv_sim_uncertainties/
#rm -rf /lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/uncertainties_production//m900_d1p5_s0p2_RHQDF_inclus/
