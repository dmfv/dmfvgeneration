#!/bin/bash                                                                                                                                                                                               
gensubmitter=submit.sh
tmpFolder=/lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/uncertainties_production/

tmpFolder=/lustre/fs24/group/atlas/alopezso/DMFV/xsecProportion/

mMeds=( 300 400 500 600 700 800 900 1000 1200 1400 1600 1800 2000 )
dcoups=( 0p0 0p25 0p5 0p75 1p0 1p25 1p5 1p75 2p0 )

#dcoups=( 0p25 )
mMeds=( 600 800 1200 1400 )
dcoups=( 0p25 1p0 2p0 )

#uncs=( scup scdw alup aldw )
#uncs=(scup scdw py3adw py3bdw py3cdw py1dw py2dw py3aup py3bup py3cup py1up py2up nom)
#uncs=(nom)
uncs=(inclus)
uncs=(schan pp fgen)
#uncs=(schan)

sins=( 0p1 0p2 0p25 )
benchmarks=( LHQDF2 LHQDF1 RHSFF RHQDF )
nevents=10000

#MadGraphConfig=MadGraphControl_DMFV.py
MadGraphConfig=MadGraphControl_DMFV_final.py
typoJOFolder=$PWD/900003 # 900003 is for decays without MadSpin, 900004 with MadSpin, 900005 for MG decays
stem=mc.MGPy8EG_A14N30NLO_m1500_d1p0_s0p2_LHQDF2_1LorMET.py

outSpace=/lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/uncertainties_production/
outSpace=/lustre/fs24/group/atlas/alopezso/DMFV/xsecProportion/
lustreSpace=/lustre/fs22/group/atlas/alopezso/tcMeT/dmfv_sim_uncertainties/

#outSpace=/lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/LHEfiles/
mkdir -p $outSpace/
mkdir -p $lustreSpace/
mkdir -p $tmpFolder

DSID=900000

date=22112023

mkdir -p logs/
rm $gensubmitter
chmod 777 $gensubmitter
echo "#!/bin/bash" > $gensubmitter
for mMed in ${mMeds[*]}
do
    for dcoup in ${dcoups[*]}
    do
	for sin in ${sins[*]}
	do
	    for unc in ${uncs[*]}
	    do
		for benchmark in ${benchmarks[*]}
		do
		    #if [[ $benchmark != LHQDF* ]];
		    #then
		    #    continue
		    #fi
		    
		    if [[ $benchmark == LHQDF1 ]];
		    then
			if [[ $sin != 0p1 ]];
			then
			    continue
			fi
		    elif [[ $benchmark == LHQDF2 ]];
		    then
			if [[ $sin != 0p2 ]];
			then
			    continue
			fi
		    elif [[ $benchmark == RHSFF ]];
		    then
			if [[ $sin != 0p25 ]];
			then
			    continue
			fi
		    elif [[ $benchmark == RHQDF ]];
		    then
			if [[ $sin != 0p2 ]];
			then
			    continue
		    fi
		    fi
		    echo "Processing MMED : ${mMed} dLL : ${dcoup}, st : $sin and benchmark : $benchmark and uncertainty: $unc"
		    #echo "benchmarks[\"$benchmark\"][\"$DSID\"]=[$mMed,$dcoup]"
		    file=${stem%A14N30NLO_*}A14_m${mMed}_d${dcoup}_s${sin}_${benchmark}_1LorMET_$unc.py
		    
		    ## Building the specific jobOption                                                                                                                                                                
		    rm -rf $PWD/$DSID
		    mkdir -p $PWD/$DSID
		    echo "include(\"$MadGraphConfig\")" > $PWD/$DSID/$file 
		    #cp $typoJOFolder/$stem $PWD/$DSID/$file
		    
		    submitter=logs/submit_m${mMed}_d${dcoup}_s${sin}_${benchmark}_${unc}.submit
		    jobOption=logs/generate_m${mMed}_d${dcoup}_s${sin}_${benchmark}_${unc}.sh
		    rm $submitter $jobOption
		    echo "" > $submitter
		    echo "#!/bin/bash" > $jobOption
		    echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase" >> $jobOption
		    echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh " >> $jobOption
		    #echo "export ATHENA_PROC_NUMBER=8" >> $jobOption
		    echo "asetup AthGeneration,21.6.97,here" >> $jobOption
		    echo "mkdir -p ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${unc}/" >> $jobOption
		    echo "ls" >> $jobOption
		    echo "pwd" >> $jobOption
		    echo "cd ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${unc}/" >> $jobOption
		    echo "cp -r $PWD/$DSID/ ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${unc}/" >> $jobOption
		    echo "cp $PWD/$MadGraphConfig ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${unc}/" >> $jobOption
		    echo "export PYTHONPATH=$PWD/../models/:\$PYTHONPATH" >> $jobOption                                                                                     
		    echo "Gen_tf.py --ecmEnergy=13000 --firstEvent=1  --maxEvents=$nevents --randomSeed=1234 --jo=$DSID --outputEVNTFile=${file%.py*}.root --localPath=${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${unc}/$DSID" >>$jobOption
		    echo "cp log.generate $outSpace/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${unc}.EVNT.log" >> $jobOption
		    #echo "cp ${file%.py*}.root $lustreSpace" >> $jobOption
		    echo "rm -rf ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${unc}/" >> $jobOption
		    
		    # Setting up how                                                                                                                                                                                 
		    echo "# job ${stem}_m${mMed}_d${dcoup}_s${sin}_${benchmark}_${unc}" > $submitter
		    echo "executable = /usr/bin/singularity" >> $submitter
		    echo "arguments = exec --contain --bind /afs:/afs --bind /cvmfs:/cvmfs --bind /var/lib/condor:/var/lib/condor --bind /lustre:/lustre --pwd $PWD /cvmfs/atlas.cern.ch/repo/containers/images/singularity/x86_64-centos7.img ${jobOption}" >> $submitter
		    #echo "universe = vanilla" >> $submitter
		    #echo "executable = $jobOption" >> $submitter
		    echo "+JobFlavour = \"tomorrow\"" >> $submitter
		    #echo "Requirements = HasSingularity" >> $submitter
		    #echo "+SingularityImage=\"/cvmfs/atlas.cern.ch/repo/containers/images/singularity/x86_64-centos7.img\"" >> $submitter
		    #echo "requirements = (OpSysAndVer =?= \"SL7\")" >> $submitter
		    echo "output= logs/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${unc}.EVNT.out" >> $submitter
		    echo "log= logs/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${unc}.EVNT.log" >> $submitter
		    echo "error= logs/m${mMed}_d${dcoup}_s${sin}_${benchmark}_${unc}.EVNT.err" >> $submitter
		    #echo "getenv=True" >> $submitter
		    echo "RequestCpus = 8" >> $submitter
		    echo "request_memory=1024*40" >> $submitter
		    echo "queue 1" >> $submitter
		    
		    echo "condor_submit $submitter" >> $gensubmitter
		    
		    chmod 777 $submitter 
		    chmod 777 $jobOption
		    DSID=$((DSID+1))
		    
		done
	    done
	done
    done
done
