#!/bin/bash                                                                                                                                                                                               
gensubmitter=submit.sh
tmpFolder=/tmp/alopezso/

mMeds=( 300 400 500 600 700 800 900 1000 1200 1400 1600 1800 2000 )
dcoups=( 0p0 0p25 0p5 0p75 1p0 1p25 1p5 1p75 2p0 )

#mMeds=( 600 800 1200 )
#dcoups=( 0p25 1p0 2p0 )

#mMeds=( 800 )
#dcoups=( 0p75 )

sins=( 0p1 0p2 0p25 )
benchmarks=( LHQDF2 LHQDF1 RHSFF RHQDF )
nevents=10000

#sins=( 0p25 )
#benchmarks=(RHSFF)
#mMeds=( 300 )
#dcoups=(2p0)

MadGraphConfig=MadGraphControl_DMFV_web.py
#MadGraphConfig=MadGraphControl_DMFV_Decays.py
#MadGraphConfig=MadGraphControl_DMFV_MadSpin.py
typoJOFolder=$PWD/900003 # 900003 is for decays without MadSpin, 900004 with MadSpin, 900005 for MG decays
stem=mc.MGPy8EG_A14N30NLO_m1500_d1p0_s0p2_LHQDF2_1LorMET.py

outSpace=/lustre/fs22/group/atlas/alopezso/DMFV/final_xsections/goodmass_production/
lustreSpace=/lustre/fs22/group/atlas/alopezso/tcMeT/dmfv_sim_correctmasses/

#outSpace=$PWD/../xsections/production_mgdecays/
#lustreSpace=/lustre/fs22/group/atlas/alopezso/tcMeT/dmfv_sim_mgdecays/
tmpFolder=/lustre/fs24/group/atlas/alopezso/DMFV/
lustreSpace=/lustre/fs24/group/atlas/alopezso/DMFV/
outSpace=/lustre/fs24/group/atlas/alopezso/DMFV/

mkdir -p $outSpace/

DSID=500000

date=22112023

mkdir -p logs/
rm $gensubmitter
chmod 777 $gensubmitter
echo "#!/bin/bash" > $gensubmitter
for mMed in ${mMeds[*]}
do
    for dcoup in ${dcoups[*]}
    do
	for sin in ${sins[*]}
	do
	    for benchmark in ${benchmarks[*]}
	    do
		#if [[ $benchmark != LHQDF* ]];
		#then
		#    continue
		#fi

		if [[ $benchmark == LHQDF1 ]];
		then
		    if [[ $sin != 0p1 ]];
		    then
			continue
		    fi
		elif [[ $benchmark == LHQDF2 ]];
		then
		    if [[ $sin != 0p2 ]];
		    then
			continue
		    fi
		elif [[ $benchmark == RHSFF ]];
		then
		    if [[ $sin != 0p25 ]];
		    then
			continue
		    fi
		elif [[ $benchmark == RHQDF ]];
		then
		    if [[ $sin != 0p2 ]];
		    then
			continue
		    fi
		fi
		echo "Processing MMED : ${mMed} dLL : ${dcoup}, st : $sin and benchmark : $benchmark"
		#echo "benchmarks[\"$benchmark\"][\"$DSID\"]=[$mMed,$dcoup]"
		file=${stem%A14N30NLO_*}A14N30NLO_m${mMed}_d${dcoup}_s${sin}_${benchmark}_1LorMET.py
		
		## Building the specific jobOption                                                                                                                                                                
		rm -rf $PWD/$DSID
		mkdir -p $PWD/$DSID
		echo "include(\"$MadGraphConfig\")" > $PWD/$DSID/$file 
		#cp $typoJOFolder/$stem $PWD/$DSID/$file
		
		submitter=logs/submit_m${mMed}_d${dcoup}_s${sin}_${benchmark}.submit
		jobOption=logs/generate_m${mMed}_d${dcoup}_s${sin}_${benchmark}.sh
		rm $submitter $jobOption
		echo "" > $submitter
		echo "#!/bin/bash" > $jobOption
		echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase" >> $jobOption
		echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh " >> $jobOption
		echo "asetup AthGeneration,21.6.97,here" >> $jobOption
		echo "mkdir -p ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}/" >> $jobOption
		echo "ls" >> $jobOption
		echo "pwd" >> $jobOption
		echo "cd ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}/" >> $jobOption
		echo "cp -r $PWD/$DSID/ ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}/" >> $jobOption
		echo "cp $PWD/$MadGraphConfig ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}/" >> $jobOption
		echo "export PYTHONPATH=$PWD/../models/:\$PYTHONPATH" >> $jobOption                                                                                     
		echo "Gen_tf.py --ecmEnergy=13000 --firstEvent=1  --maxEvents=$nevents --randomSeed=1234 --jo=$DSID --outputEVNTFile=${file%.py*}.root --localPath=${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}/$DSID" >>$jobOption
		echo "cp log.generate $outSpace/m${mMed}_d${dcoup}_s${sin}_${benchmark}.EVNT.log" >> $jobOption
		#echo "cp ${file%.py*}.root $lustreSpace" >> $jobOption
		echo "rm -rf ${tmpFolder}/m${mMed}_d${dcoup}_s${sin}_${benchmark}/" >> $jobOption
		
		# Setting up how                                                                                                                                                                                 
		echo "# job ${stem}_m${mMed}_d${dcoup}_s${sin}_${benchmark}" >> $submitter
		echo "executable = /usr/bin/singularity" >> $submitter
                echo "arguments = exec --contain --bind /afs:/afs --bind /cvmfs:/cvmfs --bind /var/lib/condor:/var/lib/condor --bind /lustre:/lustre --pwd $PWD /cvmfs/atlas.cern.ch/repo/containers/images/singularity/x86_64-centos7.img\
 ${jobOption}" >> $submitter
		#echo "universe = vanilla" >> $submitter
		#echo "executable = $jobOption" >> $submitter
		echo "+JobFlavour = \"tomorrow\"" >> $submitter
		echo "output= logs/m${mMed}_d${dcoup}_s${sin}_${benchmark}.EVNT.out" >> $submitter
		echo "log= logs/m${mMed}_d${dcoup}_s${sin}_${benchmark}.EVNT.log" >> $submitter
		echo "error= logs/m${mMed}_d${dcoup}_s${sin}_${benchmark}.EVNT.err" >> $submitter
		#echo "getenv=True" >> $submitter
		echo "request_memory=1024*40" >> $submitter
		echo "RequestCpus = 1" >> $submitter
		echo "queue 1" >> $submitter
		
		echo "condor_submit $submitter" >> $gensubmitter
		
		chmod 777 $submitter 
		chmod 777 $jobOption
		DSID=$((DSID+1))
		
	    done
	done
    done
done
