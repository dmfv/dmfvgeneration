#!/bin/bash


mMeds=( 350 500 750 1000 1250 1500 1750 2000 )
mMeds=( 500 1000 1500 2000 )
mMeds=( 250 750 1250 1750 )
dcoups=( 0p0 0p2 0p4 0p6 0p8 1p0 1p2 1p4 1p6 1p8 2p0)
dcoups=( 0p0 0p5 1p0 1p5 2p0 )
dcoups=( 0p25 0p75 1p25 1p75 )
sins=( 0p1 0p2 0p25)
benchmarks=( LHQDF2 LHQDF1 RHSFF RHQDF)

nevents=10000
gensubmitter=submitgrid.sh


MadGraphConfig=MadGraphControl_DMFV.py
typoJOFolder=$PWD/900003
stem=mc.MGPy8EG_A14N30NLO_m1500_d1p0_s0p2_LHQDF2_1LorMET.py

DSID=400081

date=15082023

echo "#!/bin/bash" > $gensubmitter
for mMed in ${mMeds[*]}
do
    for dcoup in ${dcoups[*]}
    do
        for sin in ${sins[*]}
        do
            for benchmark in ${benchmarks[*]}
            do
		if [[ $benchmark == LHQDF1 ]];
		then
		    if [[ $sin != 0p1 ]];
		    then
			continue
		    fi
		elif [[ $benchmark == LHQDF2 ]];
		then
		    if [[ $sin != 0p2 ]];
		    then
			continue
		    fi
		elif [[ $benchmark == RHSFF ]];
		then
		    if [[ $sin != 0p25 ]];
		    then
			continue
		    fi
		elif [[ $benchmark == RHQDF ]];
		then
		    if [[ $sin != 0p2 ]];
		    then
			continue
		    fi
		fi

                #echo "Processing MMED : ${mMed} dLL : ${dcoup}, st : $sin and benchmark : $benchmark"
		echo "benchmarks[\"${benchmark}\"][\"${DSID}\"] = [${mMed},${dcoup}]"
		file=${stem%A14N30NLO_*}A14N30NLO_m${mMed}_d${dcoup}_s${sin}_${benchmark}_MET150.py
		
		## Building the specific jobOption
		mkdir -p $PWD/$DSID
		cp $typoJOFolder/$stem $PWD/$DSID/$file
		echo "pathena --trf \"Gen_tf.py --ecmEnergy=13000 --firstEvent=1  --maxEvents=$nevents --randomSeed=%RNDM:0 --jo=$DSID --outputEVNTFile=%OUT.test.root \" --split 5 --outDS=user.alopezso.${date}.generation21.2.97.${DSID} --extFile=DMFV_LH,DMFV_RH" >>$gensubmitter
		DSID=$((DSID+1))
	    done
	done
    done
done

