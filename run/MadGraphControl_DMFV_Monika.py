import math,sys
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment 

phys_short = get_physics_short()
mphi = float(phys_short.split('_')[2].replace("m",""))
dcoupling=float(phys_short.split('_')[3].replace("d","").replace("p","."))
sin=float(phys_short.split('_')[4].replace("s","").replace("p","."))
benchmark=phys_short.split('_')[5]

evgenLog.info('Processing DMFV model with params: (mphi, D11, sinp) = (%e,%e,%e)' %(mphi,dcoupling,sin))


model = None

if any(x == benchmark for x in ["RHSFF","RHQDF"]):
    model = "DMFV_RH"
elif any(x == benchmark for x in ["LHQDF1","LHQDF2"]):
    model = "DMFV_LH"
else:
    print("I don't recognize the benchmark name used. Exiting.")
    sys.exit(-1)

gen_process = None
if any(x == benchmark for x in ["RHSFF","RHQDF"]):
    gen_process = """
    import model ./Monikas/fdm-models/1702.08457/%s
    define p = g u c d s u~ c~ d~ s~ b b~
    define j = g u c d s u~ c~ d~ s~ b b~
    define chi = chiu chic chit
    define chi~ = chiu~ chic~ chit~
    define ul = u c u~ c~ t t~
    define chiul = chiu chic chiu~ chic~ chit~ chit
    define ulight = u c u~ c~
    define chilight = chiu chic chiu~ chic~
    define chita = chit chit~
    define phia = phi phi~
    define tta = t t~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define ll = l+ l-
    define vall = vl vl~
    define bj = b b~
    define wpm = w+ w-
    define all = j ll vall
    generate p p > phia tta chita, (phia > ulight chilight)
    output -f
    """%("DMFV_uR_D_UFO")
elif any(x == benchmark for x in ["LHQDF1","LHQDF2"]):
    gen_process = """
    import model ./Monikas/fdm-models/1711.10493/%s
    define p = g u c d s u~ c~ d~ s~ b b~
    define j = g u c d s u~ c~ d~ s~ b b~
    define chi = chiu chic chit
    define chi~ = chiu~ chic~ chit~
    define ulight = u c u~ c~
    define chilight = chiu chic chiu~ chic~
    define ul = u c u~ c~ t t~
    define chiul = chiu chic chiu~ chic~ chit chit~
    define chita = chit chit~
    define phia = phiup phiup~
    define tta = t t~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define ll = l+ l-
    define vall = vl vl~
    define bj = b b~
    define wpm = w+ w-
    define all = j ll vall
    generate p p > phia tta chita, (phia > ulight chilight) ## resonant production for c-jet, not necessarily for top production
    output -f
    """%("DMFV_qL_D_UFO")
else:
    print("I don't recognize the benchmark name used. Exiting.")
    sys.exit(-1)


if '1LorMET' in phys_short.split('_')[-1]:
    include ( 'GeneratorFilters/LeptonFilter.py' )
    filtSeq.LeptonFilter.Ptcut  = 20000.
    filtSeq.LeptonFilter.Etacut = 2.8

    include('GeneratorFilters/MissingEtFilter.py')
    filtSeq.MissingEtFilter.METCut = 0.0

    filtSeq += MissingEtFilter("MissingEtFilterHard")
    filtSeq.MissingEtFilterHard.METCut = 150000.

    filtSeq.Expression = "(LeptonFilter and MissingEtFilter) or MissingEtFilterHard"
    evt_multiplier = 5 
elif 'MET' in phys_short.split('_')[-1]:
    include ( 'GeneratorFilters/MissingEtFilter.py' )

    metFilter = phys_short.split('_')[-1]
    metFilter = int(metFilter.split("MET")[1].split("_")[0])

    print "Using MET Filter: " + str(metFilter)
    filtSeq.MissingEtFilter.METCut = metFilter*GeV
    evt_multiplier = metFilter / 10
else:
    print "No MET Filter applied"


if evt_multiplier>0:
  if runArgs.maxEvents>0:
      nevents=runArgs.maxEvents*evt_multiplier
  else:    
      nevents=evgenConfig.nEventsPerJob*evt_multiplier
        
run_settings = {'lhe_version':'3.0',
                'pdgs_for_merging_cut': '1, 2, 3, 4, 5, 21',
                'asrwgtflavor': 5,
                'maxjetflavor':5,  #5 flavor scheme
                'ptj':20.,
                #'parton_shower':'PYTHIA8'
            }
#run_settings = {'lhe_version':'3.0',
#                'pdlabel'    : "'lhapdf'",
#                #'lhaid'      : 263000, #Originally using 260000
#                'lhaid'      : 263000, #Originally using 260000
#                'pdgs_for_merging_cut': '1, 2, 3, 4, 5, 21',
#                'asrwgtflavor': 5,
#                'maxjetflavor':5,  #5 flavor scheme
#                'use_syst': 'F',  #to avoid storing systematic uncertainties.
#                'ptj':20.,
#                #'parton_shower':'PYTHIA8'
#            }
_nQuarksMerge = 5

#run_settings['event_norm']='sum'
run_settings['nevents'] = nevents

# Set up the process
process_dir = new_process(gen_process)
# Set up the run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)

## Modifiy the parameter card
params = {}

if benchmark=="RHSFF":
    params["MASS"] = {'9000005':mphi,'9000006':220,'9000007':220,'9000008':200}
    params["FRBLOCK"] = {'thet12':0.0,'thet23':0.0,'thet13':math.asin(sin),'del12':0.0,'del23':0.0,'del13':0.0,'lam1':dcoupling,'lam2':dcoupling,'lam3':dcoupling+1.0} ## Consider no CP-violation phase
elif benchmark=="RHQDF":
    params["MASS"] = {'9000005':mphi,'9000006':151.5,'9000007':151.5,'9000008':150}
    params["FRBLOCK"] = {'thet12':0.0,'thet23':0.0,'thet13':math.asin(sin),'del12':0.0,'del23':0.0,'del13':0.0,'lam1':dcoupling,'lam2':dcoupling,'lam3':dcoupling+0.2} ## Consider no CP-violation phase
elif benchmark=="LHQDF1":
    params["MASS"] = {'9000005':mphi,'9000006':mphi,'9000007':151.5,'9000008':151.5,'9000009':150}
    params["FRBLOCK"] = {'thet12':0.0,'thet23':0.0,'thet13':math.asin(sin),'del12':0.0,'del23':0.0,'del13':0.0,'lam1':dcoupling,'lam2':dcoupling,'lam3':dcoupling+0.1} ## Consider no CP-violation phase
elif benchmark=="LHQDF2":
    params["MASS"] = {'9000005':mphi,'9000006':mphi,'9000007':454.5,'9000008':454.5,'9000009':450}
    params["FRBLOCK"] = {'thet12':0.0,'thet23':0.0,'thet13':math.asin(sin),'del12':0.0,'del23':0.0,'del13':0.0,'lam1':dcoupling,'lam2':dcoupling,'lam3':dcoupling+0.2} ## Consider no CP-violation phase
else:
    print("I don't recognize the benchmark name used. Exiting.")
    sys.exit(-1)

if "RH" in model:
    params['DECAY']={'9000005':'DECAY  9000005 Auto'}
elif "LH" in model:
    params['DECAY']={'9000005':'DECAY  9000005 Auto','9000006':'DECAY  9000006 Auto'}
else:
    print("Aha! Got you ! I don't know how you got so far without me breaking, but here it is. Use a valid DMFV model. Exiting.")
    sys.exit(-1)

modify_param_card(process_dir=process_dir,params=params)
print_cards()

# Generate the events
generate(process_dir=process_dir,runArgs=runArgs)

# Remember to set saveProcDir to FALSE before sending for production!!
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

# Set the evgen metadata
evgenConfig.description = 'Flavour violating DM model, m_phi = %s GeV and DM-SM coupling of %.1f'%(mphi,dcoupling)
evgenConfig.keywords = ["exotic","BSM","WIMP", "SUSY"]
evgenConfig.contact = ["Alvaro Lopez Solis <alvaro.lopez.solis@cern.ch>"]

#comment = '''
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Reset the number of processes for Pythia8
check_reset_proc_number(opts)


if "RH" in model:
    #genSeq.Pythia8.ParticleData="ParticleData_RH.xml"
    genSeq.Pythia8.Commands +=["Init:showAllParticleData = on"]
    genSeq.Pythia8.Commands+=["9000005:new = phi phi~ 2 2 1 %s "%params["MASS"]["9000005"]]
    genSeq.Pythia8.Commands+=["9000006:new = chiu chiu~ 2 0 0 %s "%params["MASS"]["9000006"]]
    genSeq.Pythia8.Commands+=["9000007:new = chic chic~ 2 0 0 %s "%params["MASS"]["9000007"]]
    genSeq.Pythia8.Commands+=["9000008:new = chit chit~ 2 0 0 %s "%params["MASS"]["9000008"]]
    genSeq.Pythia8.Commands+=[
        "9000006:isVisible = false",
        "9000007:isVisible = false",
        "9000008:isVisible = false",
        "9000006:mayDecay = false",
        "9000007:mayDecay = false",
        "9000008:mayDecay = false",
    ]
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    bonus_file = open('pdg_extras.dat','w')
    #  The most important number is the first: the PDGID of the particle
    bonus_file.write( '9000006 DM up {} (MeV/c) 0\n'.format(params["MASS"]["9000006"]*1000.0))
    bonus_file.write( '-9000006 Anti-DM up {} (MeV/c) 0\n'.format(params["MASS"]["9000006"]*1000.0))
    bonus_file.write( '9000007 DM charm {} (MeV/c) 0\n'.format(params["MASS"]["9000007"]*1000.0))
    bonus_file.write( '-9000007 Anti-DM charm {} (MeV/c) 0\n'.format(params["MASS"]["9000007"]*1000.0))
    bonus_file.write( '9000008 DM top {} (MeV/c) 0\n'.format(params["MASS"]["9000008"]*1000.0))
    bonus_file.write( '-9000008 Anti-DM top {} (MeV/c) 0\n'.format(params["MASS"]["9000008"]*1000.0))
    bonus_file.close()
    testSeq.TestHepMC.G4ExtraWhiteFile='pdg_extras.dat'
elif "LH" in model:
    genSeq.Pythia8.Commands +=["Init:showAllParticleData = on"]
    genSeq.Pythia8.Commands+=["9000005:new = phiup phiup~ 1 2 1 %s "%params["MASS"]["9000005"]]
    genSeq.Pythia8.Commands+=["9000006:new = phidown phidown~ 1 -1 1 %s "%params["MASS"]["9000006"]]
    genSeq.Pythia8.Commands+=["9000007:new = chiu chiu~ 2 0 0 %s "%params["MASS"]["9000007"]]
    genSeq.Pythia8.Commands+=["9000008:new = chic chic~ 2 0 0 %s "%params["MASS"]["9000008"]]
    genSeq.Pythia8.Commands+=["9000009:new = chit chit~ 2 0 0 %s "%params["MASS"]["9000009"]]
    genSeq.Pythia8.Commands+=[
        "9000007:isVisible = false",
        "9000008:isVisible = false",
        "9000009:isVisible = false",
        "9000007:mayDecay = false",
        "9000008:mayDecay = false",
        "9000009:mayDecay = false",
    ]
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    bonus_file = open('pdg_extras.dat','w')
    #  The most important number is the first: the PDGID of the particle
    bonus_file.write( '9000007 DM up {} (MeV/c) 0\n'.format(params["MASS"]["9000007"]*1000.0))
    bonus_file.write( '-9000007 Anti-DM up {} (MeV/c) 0\n'.format(params["MASS"]["9000007"]*1000.0))
    bonus_file.write( '9000008 DM charm {} (MeV/c) 0\n'.format(params["MASS"]["9000008"]*1000.0))
    bonus_file.write( '-9000008 Anti-DM charm {} (MeV/c) 0\n'.format(params["MASS"]["9000008"]*1000.0))
    bonus_file.write( '9000009 DM top {} (MeV/c) 0\n'.format(params["MASS"]["9000009"]*1000.0))
    bonus_file.write( '-9000009 Anti-DM top {} (MeV/c) 0\n'.format(params["MASS"]["9000009"]*1000.0))
    bonus_file.close()
    testSeq.TestHepMC.G4ExtraWhiteFile='pdg_extras.dat'
else:
    print("Aha! Got you ! I don't know how you got so far without me breaking, but here it is. Use a valid DMFV model. Exiting.")
    sys.exit(-1)


